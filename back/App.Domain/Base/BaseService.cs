﻿using App.Common.Extensions;
using App.Domain.Def.Base;
using App.Repository.Def.Base;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace App.Domain.Base
{
    public abstract class BaseSharedService<TDTO, TPOCO> : IBaseSharedService<TDTO>
        where TDTO : class
        where TPOCO : class
    {
        protected ISharedRepository<TPOCO> _repository;

        protected readonly string _cacheKey = string.Format("list_{0}", typeof(TPOCO).Name);

        protected BaseSharedService() { }

        protected BaseSharedService(ISharedRepository<TPOCO> repository)
        {
            _repository = repository;
        }

        public virtual void Create(TDTO dto)
        {
            TPOCO entity = Mapper.Map<TPOCO>(dto);
            _repository.Insert(entity);
        }

        public virtual void Delete(object id)
        {
            _repository.Delete(id);
        }

        public virtual void Update(object id, TDTO dto)
        {
            TPOCO entity = Mapper.Map<TPOCO>(dto);
            _repository.Update(entity, id);
        }

        public async virtual Task CreateAsync(TDTO dto)
        {
            TPOCO entity = Mapper.Map<TPOCO>(dto);
            _repository.Insert(entity);
        }

        public async virtual Task DeleteAsync(int id)
        {
            _repository.Delete(id);
        }

        public async virtual Task UpdateAsync(int id, TDTO dto)
        {
            TPOCO entity = Mapper.Map<TPOCO>(dto);
            _repository.Update(entity, id);
        }

        public virtual TDTO FindById(int id)
        {
            return Mapper.Map<TDTO>(_repository.FindById(id));
        }

        public virtual IEnumerable<TDTO> GetAll()
        {
            List<TDTO> list;
            list = Mapper.Map<List<TDTO>>(_repository.All.ToList());
            return list;
        }

        public virtual IEnumerable<TDTO> GetAllNoCache()
        {
            List<TPOCO> list;
            list = _repository.All.ToList();
            return list.Select(data => Mapper.Map<TDTO>(data));
        }

        public object GetProperties()
        {
            List<PropertyInfoContract> response = new List<PropertyInfoContract>();
            Type type = typeof(TDTO);
            IEnumerable<PropertyInfo> props = type.GetProperties();

            foreach (var prop in props)
            {
                var _attributes = prop.GetCustomAttributes(typeof(CustomPropertiesInfo), false);
                CustomPropertiesInfo attributes = _attributes.Length > 0?  (CustomPropertiesInfo)_attributes.First() : null;

                PropertyInfoContract model = new PropertyInfoContract
                {
                    Column = prop.Name,
                    DisplayName = attributes != null ? attributes.Name : prop.Name,
                    Visible = attributes != null ? attributes.InTable : false,
                    IsForeign = attributes != null ? attributes.IsForeign : false,
                    ForeignKey = attributes != null ? attributes.ForeignKey : string.Empty,
                    ForeignValue = attributes != null ? attributes.ForeignValue : string.Empty,
                    ForeignController = attributes != null ? attributes.ForeignController : string.Empty,
                    IsCheckBox = attributes != null ? attributes.IsCheckBox : false,
                    Required = attributes != null ? attributes.Required : false,
                    PrimaryKey = attributes != null ? attributes.PrimaryKey : false,

                };
                response.Add(model);
            }
            return response;
        }
    }
}






