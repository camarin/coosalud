﻿namespace App.Domain.Base
{
    public class PropertyInfoContract
    {
        public string Column
        {
            get;
            set;
        }


        public string DisplayName
        {
            get;
            set;
        }

        public bool Visible
        {
            get;
            set;
        }

        public bool IsForeign
        {
            get;
            set;
        }

        public string ForeignKey
        {
            get;
            set;
        }

        public string ForeignValue
        {
            get;
            set;
        }

        public string ForeignController
        {
            get;
            set;
        }

        public bool IsCheckBox
        {
            get;
            set;
        }

        public bool IsDateTime
        {
            get;
            set;
        }

        public bool Required
        {
            get;
            set;
        }

        public bool PrimaryKey { get; set; }
    }
}
