using App.Domain.Def.Definitions.Sys;
using App.Domain.Def.DTO.Sys;
using App.Repository.Def.Definitions.Sys;
using App.Repository.Def.POCOS.Sys;

namespace App.Domain.Services.Implementations.Sys
{
    public class DocumentTypeService : Base.BaseSharedService<DocumentTypeDTO, DocumentTypePOCO>, IDocumentTypeService
    {
        public DocumentTypeService(IDocumentTypeRepository DocumentTypeRepository)
            : base(DocumentTypeRepository)
        {
        }
    }
}