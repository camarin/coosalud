using App.Domain.Def.Definitions.Auth;
using App.Domain.Def.DTO.Auth;
using App.Repository.Def.Definitions.Auth;
using App.Repository.Def.POCOS.Auth;

namespace App.Domain.Services.Implementations.Auth
{
    public class ModuleService : Base.BaseSharedService<ModuleDTO, ModulePOCO>, IModuleService
    {
        public ModuleService(IModuleRepository ModuleRepository)
            : base(ModuleRepository)
        {
        }
    }
}