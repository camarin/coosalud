using App.Domain.Def.Definitions.Auth;
using App.Domain.Def.DTO.Auth;
using App.Repository.Def.Definitions.Auth;
using App.Repository.Def.POCOS.Auth;

namespace App.Domain.Services.Implementations.Auth
{
    public class RoleModuleService : Base.BaseSharedService<RoleModuleDTO, RoleModulePOCO>, IRoleModuleService
    {
        public RoleModuleService(IRoleModuleRepository RoleModuleRepository)
            : base(RoleModuleRepository)
        {
        }
    }
}