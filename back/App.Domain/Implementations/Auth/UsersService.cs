using App.Domain.Def.Contracts.Users;
using App.Domain.Def.Definitions.Auth;
using App.Domain.Def.DTO.Auth;
using App.Repository.Def.Definitions.Auth;
using App.Repository.Def.Definitions.Security;
using App.Repository.Def.POCOS.Auth;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.Domain.Services.Implementations.Auth
{
    public class UsersService : Base.BaseSharedService<UsersDTO, UsersPOCO>, IUsersService
    {
        ILdapConnectionRepository ldapConnectionRepository;
        IUsersRepository usersRepository;
        public UsersService(IUsersRepository UsersRepository, ILdapConnectionRepository LdapConnectionRepository)
            : base(UsersRepository)
        {
            ldapConnectionRepository = LdapConnectionRepository;
            usersRepository = UsersRepository;
        }

        /// <summary>
        /// Return all users
        /// </summary>
        /// <returns></returns>
        public IEnumerable<VwUsersDTO> GetVwAll()
        {
            return Mapper.Map<IEnumerable<VwUsersDTO>>(this.usersRepository.GetVwAll());
        }


        /// <summary>
        /// Login the User
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public LoginUserResponseContract Login(LoginUserRequestContract request)
        {
            request.UserName = request.UserName.ToLower();
            UsersDTO user = GetAll().FirstOrDefault(m => m.UserName == request.UserName);
            if (user != null && ldapConnectionRepository.ValidateIfUserExist(request.UserName, request.Password))
            {
                byte[] ticks = BitConverter.GetBytes(DateTime.Now.Ticks);
                string token = Convert.ToBase64String(ticks);
                usersRepository.UpdateToken(user.UserName, token);

                return new LoginUserResponseContract
                {
                    UserName = user.UserName,
                    Token = token
                };
            }
            throw new Exception("El usuario no tiene permisos para ingresar a esta aplicaci�n.");
        }


        public bool RequestPermissions(PermissionsRequestContract request)
        {
            UsersDTO user = GetAll().FirstOrDefault(m => m.UserName == request.UserName);
            //if (user != null && user.SessionHash == request.Token)
            //{
            //    bool modules = modulesRepository.GetByUserId(user.UserId).Any(m => m.ModuleKey.Equals(request.ModuleKey));
            //    if (modules)
            //    {
            //        return true;
            //    }
            //    else
            //    {
            //        throw new Exception("Usted no tiene permisos para acceder a este m�dulo.");
            //    }
            //}
            return false;
        }
    }
}