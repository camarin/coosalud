using App.Domain.Def.Contracts.Auth;
using App.Domain.Def.Definitions.Auth;
using App.Domain.Def.DTO.Auth;
using App.Repository.Def.Definitions.Auth;
using App.Repository.Def.POCOS.Auth;
using System.Collections.Generic;
using System.Linq;

namespace App.Domain.Services.Implementations.Auth
{
    public class RoleService : Base.BaseSharedService<RoleDTO, RolePOCO>, IRoleService
    {
        private readonly IRoleModuleRepository roleModuleRepository;
        public RoleService(IRoleRepository RoleRepository, IRoleModuleRepository RoleModuleRepository)
            : base(RoleRepository)
        {
            this.roleModuleRepository = RoleModuleRepository;
        }

        public void EditWithModules(RoleModulesRequestContract request)
        {
            IEnumerable<RoleModulePOCO> rolesModule = this.roleModuleRepository.All.Where(m => m.RoleId == request.Role.RoleId);
            foreach (RoleModulePOCO item in rolesModule)
            {
                this.roleModuleRepository.Delete(item.RoleModuleId);
            }

            foreach (int module in request.Modules)
            {
                this.roleModuleRepository.Insert(new RoleModulePOCO
                {
                    ModuleId = module,
                    RoleId = request.Role.RoleId
                });
            }

            Update(request.Role.RoleId, request.Role);
        }
    }
}