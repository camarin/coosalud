using App.Domain.Def.Definitions.Inv;
using App.Domain.Def.DTO.Inv;
using App.Repository.Def.Definitions.Inv;
using App.Repository.Def.POCOS.Inv;
using AutoMapper;
using System;
using System.Collections.Generic;

namespace App.Domain.Services.Implementations.Inv
{
    public class InventoryService : Base.BaseSharedService<InventoryDTO, InventoryPOCO>, IInventoryService
    {
        private readonly IInventoryRepository inventoryRepository;
        public InventoryService(IInventoryRepository InventoryRepository)
            : base(InventoryRepository)
        {
            inventoryRepository = InventoryRepository;
        }

        public override void Create(InventoryDTO dto)
        {
            dto.CreationDate = new DateTime(dto.CreationDate.Year, dto.CreationDate.Month, dto.CreationDate.Day);
            dto.StartDate = new DateTime(dto.StartDate.Year, dto.StartDate.Month, dto.StartDate.Day);
            dto.EndDate = new DateTime(dto.EndDate.Year, dto.EndDate.Month, dto.EndDate.Day);
            dto.TransferDate = null;
            base.Create(dto);
        }

        /// <summary>
        /// Retorna todos los documentos
        /// </summary>
        /// <returns></returns>
        public IEnumerable<VwInventoryDTO> GetVwAll()
        {
            return Mapper.Map<IEnumerable<VwInventoryDTO>>(this.inventoryRepository.GetVwAll());
        }
    }
}