using App.Domain.Def.Definitions.Inv;
using App.Domain.Def.DTO.Inv;
using App.Repository.Def.Definitions.Inv;
using App.Repository.Def.POCOS.Inv;

namespace App.Domain.Services.Implementations.Inv
{
    public class InventoryStateService : Base.BaseSharedService<InventoryStateDTO, InventoryStatePOCO>, IInventoryStateService
    {
        public InventoryStateService(IInventoryStateRepository InventoryStateRepository)
            : base(InventoryStateRepository)
        {
        }
    }
}