using App.Domain.Def.Definitions.Trd;
using App.Domain.Def.DTO.Trd;
using App.Repository.Def.Definitions.Trd;
using App.Repository.Def.POCOS.Trd;

namespace App.Domain.Services.Implementations.Trd
{
    public class EntityService : Base.BaseSharedService<EntityDTO, EntityPOCO>, IEntityService
    {
        public EntityService(IEntityRepository EntityRepository)
            : base(EntityRepository)
        {
        }
    }
}