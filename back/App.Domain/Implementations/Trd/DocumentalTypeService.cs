using App.Domain.Def.Definitions.Trd;
using App.Domain.Def.DTO.Trd;
using App.Repository.Def.Definitions.Trd;
using App.Repository.Def.POCOS.Trd;

namespace App.Domain.Services.Implementations.Trd
{
    public class DocumentalTypeService : Base.BaseSharedService<DocumentalTypeDTO, DocumentalTypePOCO>, IDocumentalTypeService
    {
        public DocumentalTypeService(IDocumentalTypeRepository DocumentalTypeRepository)
            : base(DocumentalTypeRepository)
        {
        }
    }
}