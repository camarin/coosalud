using System.Collections.Generic;
using App.Domain.Def.Definitions.Trd;
using App.Domain.Def.DTO.Trd;
using App.Repository.Def.Definitions.Trd;
using App.Repository.Def.POCOS.Trd;
using AutoMapper;

namespace App.Domain.Services.Implementations.Trd
{
    public class DependenceService : Base.BaseSharedService<DependenceDTO, DependencePOCO>, IDependenceService
    {
        private readonly IDependenceRepository dependenceRepository;
        public DependenceService(IDependenceRepository DependenceRepository)
            : base(DependenceRepository)
        {
            this.dependenceRepository = DependenceRepository;
        }

        /// <summary>
        /// Retorna todos las dependencias con la entidad a la que pertenece
        /// </summary>
        /// <returns></returns>
        public IEnumerable<VwDependenceDTO> GetVwAll()
        {
            return Mapper.Map<IEnumerable<VwDependenceDTO>>(this.dependenceRepository.GetVwAll());
        }
    }
}