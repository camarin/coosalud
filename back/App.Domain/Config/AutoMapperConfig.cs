﻿using App.Repository.Config;
using AutoMapper;
using AutoMapper.Data;

namespace App.Domain.Config
{
    public class AutoMapperConfig
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg => {
                cfg.AddDataReaderMapping();
                cfg.CreateMissingTypeMaps = true; 
                cfg.AddProfile<RepositoyMapperProfile>();
            });
        }
    }
}
