﻿using App.Domain.Services.Implementations.Auth;
using Autofac;


namespace App.Domain.Config
{
    public static class AutofacConfig
    {
        public static void RegisterTypes(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(UsersService).Assembly)
                   .Where(t => t.Name.EndsWith("Service",System.StringComparison.Ordinal))
                .AsImplementedInterfaces().InstancePerRequest();

            Repository.Config.AutofacConfig.RegisterTypes(builder);
        }
    }
}
