﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace App.Domain.Def.Base
{
    public interface IBaseSharedService<TDTO>
        where TDTO : class
    {
        IEnumerable<TDTO> GetAll();
        IEnumerable<TDTO> GetAllNoCache();
        void Create(TDTO dto);
        void Delete(object id);
        void Update(object id, TDTO dto);
        Task CreateAsync(TDTO dto);
        Task DeleteAsync(int id);
        Task UpdateAsync(int id, TDTO dto);
        TDTO FindById(int Id);
        object GetProperties();
    }
}
