﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace App.Domain.Def.Base
{
    public interface IBaseService<TDTO>
       where TDTO : class
    {
        IEnumerable<TDTO> GetAll();
        IEnumerable<TDTO> GetAllNoCache();
        void Create(TDTO dto);
        void Delete(object id);
        void Update(object id, TDTO dto);
        Task CreateAsync(TDTO dto);
        Task DeleteAsync(object id);
        Task UpdateAsync(object id, TDTO dto);
        TDTO FindById(object Id);
    }
}
