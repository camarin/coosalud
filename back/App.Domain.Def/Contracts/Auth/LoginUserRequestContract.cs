﻿namespace App.Domain.Def.Contracts.Users
{
    public class LoginUserRequestContract
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
