﻿using System.Collections.Generic;

namespace App.Domain.Def.Contracts.Users
{
    public class LoginUserResponseContract
    {
        public string UserName { get; set; }
        public string Token { get; set; }
        public IEnumerable<int> Divisions { get; set; }
    }
}
