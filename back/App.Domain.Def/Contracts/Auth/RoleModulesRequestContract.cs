﻿using App.Domain.Def.DTO.Auth;
using System.Collections.Generic;

namespace App.Domain.Def.Contracts.Auth
{
    public class RoleModulesRequestContract
    {
        public RoleDTO Role { get; set; }
        public IEnumerable<int> Modules { get; set; }
    }
}
