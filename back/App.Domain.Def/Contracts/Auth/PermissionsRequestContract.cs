﻿namespace App.Domain.Def.Contracts.Users
{
    public class PermissionsRequestContract
    {
        public string UserName { get; set; }
        public string Token { get; set; }
        public string ModuleKey { get; set; }
    }
}
