using System;

namespace App.Domain.Def.DTO.Inv
{
    public class InventoryDTO
    {
        public int InventoryId { get; set; }
        public int InventoryStateId { get; set; }
        public int UserId { get; set; }
        public DateTime CreationDate { get; set; }
        public int EntityId { get; set; }
        public int DependenceId { get; set; }
        public int SerieId { get; set; }
        public int SubserieId { get; set; }
        public string StorageUnit { get; set; }
        public string Tome { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string BoxCode { get; set; }
        public DateTime? TransferDate { get; set; }
    }
}
