namespace App.Domain.Def.DTO.Inv
{
    public class InventoryStateDTO
    {
        public int InventoryStateId { get; set; }
        public string InventoryStateName { get; set; }
    }
}
