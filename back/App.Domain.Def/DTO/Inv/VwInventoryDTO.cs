﻿namespace App.Domain.Def.DTO.Inv
{
    public class VwInventoryDTO : InventoryDTO
    {
        public string InventoryStateName { get; set; }
        public string SerieName { get; set; }
        public string DependenceName { get; set; }
        public string EntityName { get; set; }
        public string SubserieName { get; set; }
    }
}
