namespace App.Domain.Def.DTO.Auth
{
    public class RoleModuleDTO
    {
        public int RoleModuleId { get; set; }
        public int RoleId { get; set; }
        public int ModuleId { get; set; }
    }
}
