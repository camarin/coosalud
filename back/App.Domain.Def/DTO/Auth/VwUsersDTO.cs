namespace App.Domain.Def.DTO.Auth
{
    public class VwUsersDTO : UsersDTO
    {
        public string RoleName { get; set; }
    }
}
