namespace App.Domain.Def.DTO.Auth
{
    public class UsersDTO
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public bool State { get; set; }
        public int RoleId { get; set; }
    }
}
