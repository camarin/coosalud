namespace App.Domain.Def.DTO.Auth
{
    public class ModuleDTO
    {
        public int ModuleId { get; set; }
        public string ModuleName { get; set; }
    }
}
