namespace App.Domain.Def.DTO.Trd
{
    public class DocumentalTypeDTO
    {
        public int DocumentalTypeId { get; set; }
        public int SubserieId { get; set; }
        public string Code { get; set; }
        public string DocumentalTypeName { get; set; }
        public string GeneralWithholding { get; set; }
        public string CentralWithholding { get; set; }
        public string PaperSupport { get; set; }
        public string ElectronicSupport { get; set; }
        public string DisposalElimination { get; set; }
        public string DisposalSelection { get; set; }
        public string DisposalTotalConservation { get; set; }
        public string DisposalDigitalization { get; set; }
    }
}
