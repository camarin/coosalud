namespace App.Domain.Def.DTO.Trd
{
    public class DependenceDTO
    {
        public int DependenceId { get; set; }
        public int EntityId { get; set; }
        public string Code { get; set; }
        public string DependenceName { get; set; }
    }
}
