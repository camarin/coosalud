namespace App.Domain.Def.DTO.Trd
{
    public class SerieDTO
    {
        public int SerieId { get; set; }
        public int DependenceId { get; set; }
        public string Code { get; set; }
        public string SerieName { get; set; }
    }
}
