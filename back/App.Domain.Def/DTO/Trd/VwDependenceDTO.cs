﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Domain.Def.DTO.Trd
{
    public class VwDependenceDTO : DependenceDTO
    {
        public string EntityName { get; set; }
    }
}
