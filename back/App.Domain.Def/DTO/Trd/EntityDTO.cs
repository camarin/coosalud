namespace App.Domain.Def.DTO.Trd
{
    public class EntityDTO
    {
        public int EntityId { get; set; }
        public string EntityName { get; set; }
    }
}
