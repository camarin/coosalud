using App.Domain.Def.Base;
using App.Domain.Def.DTO.Inv;

namespace App.Domain.Def.Definitions.Inv
{
    public interface IInventoryStateService : IBaseSharedService<InventoryStateDTO>
    {
    }
}
