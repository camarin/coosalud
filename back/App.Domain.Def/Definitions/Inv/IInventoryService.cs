using App.Domain.Def.Base;
using App.Domain.Def.DTO.Inv;
using System.Collections.Generic;

namespace App.Domain.Def.Definitions.Inv
{
    public interface IInventoryService : IBaseSharedService<InventoryDTO>
    {
        IEnumerable<VwInventoryDTO> GetVwAll();
    }
}
