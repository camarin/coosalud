using App.Domain.Def.Base;
using App.Domain.Def.DTO.Trd;
using System.Collections.Generic;

namespace App.Domain.Def.Definitions.Trd
{
    public interface IDependenceService : IBaseSharedService<DependenceDTO>
    {
        IEnumerable<VwDependenceDTO> GetVwAll();
    }
}
