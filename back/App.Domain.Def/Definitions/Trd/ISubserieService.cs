using App.Domain.Def.Base;
using App.Domain.Def.DTO.Trd;

namespace App.Domain.Def.Definitions.Trd
{
    public interface ISubserieService : IBaseSharedService<SubserieDTO>
    {
    }
}
