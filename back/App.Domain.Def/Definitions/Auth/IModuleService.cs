using App.Domain.Def.Base;
using App.Domain.Def.DTO.Auth;

namespace App.Domain.Def.Definitions.Auth
{
    public interface IModuleService : IBaseSharedService<ModuleDTO>
    {
    }
}
