using App.Domain.Def.Base;
using App.Domain.Def.Contracts.Auth;
using App.Domain.Def.DTO.Auth;

namespace App.Domain.Def.Definitions.Auth
{
    public interface IRoleService : IBaseSharedService<RoleDTO>
    {
        void EditWithModules(RoleModulesRequestContract request);
    }
}
