using App.Domain.Def.Base;
using App.Domain.Def.Contracts.Users;
using App.Domain.Def.DTO.Auth;
using System.Collections.Generic;

namespace App.Domain.Def.Definitions.Auth
{
    public interface IUsersService : IBaseSharedService<UsersDTO>
    {
        LoginUserResponseContract Login(LoginUserRequestContract request);
        bool RequestPermissions(PermissionsRequestContract request);
        IEnumerable<VwUsersDTO> GetVwAll();
    }
}
