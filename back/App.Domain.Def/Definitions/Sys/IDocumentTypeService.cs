using App.Domain.Def.Base;
using App.Domain.Def.DTO.Sys;

namespace App.Domain.Def.Definitions.Sys
{
    public interface IDocumentTypeService : IBaseSharedService<DocumentTypeDTO>
    {
    }
}
