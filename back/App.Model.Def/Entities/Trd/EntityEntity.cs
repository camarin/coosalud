namespace App.Model.Def.Entities.Trd
{
    public class EntityEntity
    {
        public int EntityId { get; set; }
        public string EntityName { get; set; }
    }
}