namespace App.Model.Def.Entities.Trd
{
    public class SubserieEntity
    {
        public int SubserieId { get; set; }
        public int SerieId { get; set; }
        public string Code { get; set; }
        public string SubserieName { get; set; }
        public string Process { get; set; }
    }
}