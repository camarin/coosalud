namespace App.Model.Def.Entities.Trd
{
    public class SerieEntity
    {
        public int SerieId { get; set; }
        public int DependenceId { get; set; }
        public string Code { get; set; }
        public string SerieName { get; set; }
    }
}