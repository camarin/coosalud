namespace App.Model.Def.Entities.Trd
{
    public class DependenceEntity
    {
        public int DependenceId { get; set; }
        public int EntityId { get; set; }
        public string Code { get; set; }
        public string DependenceName { get; set; }
    }
}