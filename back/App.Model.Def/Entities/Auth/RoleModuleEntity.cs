namespace App.Model.Def.Entities.Auth
{
    public class RoleModuleEntity
    {
        public RoleModuleEntity()
        {
        }

        public int RoleModuleId { get; set; }
        public int RoleId { get; set; }
        public int ModuleId { get; set; }
    }
}