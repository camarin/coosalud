namespace App.Model.Def.Entities.Auth
{
    public class RoleEntity
    {
        public RoleEntity()
        {
        }

        public int RoleId { get; set; }
        public string RoleName { get; set; }
    }
}