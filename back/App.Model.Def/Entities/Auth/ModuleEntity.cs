namespace App.Model.Def.Entities.Auth
{
    public class ModuleEntity
    {
        public ModuleEntity()
        {
        }

        public int ModuleId { get; set; }
        public string ModuleName { get; set; }
    }
}