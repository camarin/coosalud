namespace App.Model.Def.Entities.Auth
{
    public class UsersEntity
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public bool State { get; set; }
        public int RoleId { get; set; }
    }
}