namespace App.Model.Def.Entities.Inv
{
    public class InventoryStateEntity
    {
        public int InventoryStateId { get; set; }
        public string InventoryStateName { get; set; }
    }
}