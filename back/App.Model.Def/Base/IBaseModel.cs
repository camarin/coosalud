﻿using System.Collections.Generic;
using System.Data;

namespace App.Model.Def.Base
{
    public interface IBaseModel<TEntity>
       where TEntity : class
    {
        IEnumerable<TEntity> All();
        TEntity FindById(int id);
        void Delete(object id);
        void Insert(TEntity entity);
        void Update(TEntity entity,object id);
        IEnumerable<T> Execute<T>(string query);
        IEnumerable<T> Execute<T>(string query, Dictionary<string, object> parameters);
        void ExecuteNonQuery(string query);
        void ExecuteNonQuery(string query, Dictionary<string, object> parameters);
        DataTable ExecuteQuery(string query);
    }
}
