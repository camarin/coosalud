﻿namespace App.Common.Contracts
{
    public class DeleteRequestContract
    {
        public object Id { get; set; }
    }
}
