﻿namespace App.Common.Contracts
{
    public class ResponseContract<T>
    {
        public HeaderResponseContract Header { get; set; }
        public T Data { get; set; }

        public ResponseContract(){
            Header = new HeaderResponseContract();
        }
    }
}
