﻿namespace App.Common.Contracts
{
    public class UpdateRequestContract<T>
    {
        public object Id { get; set; }
        public T Data { get; set; }

    }
}
