﻿using System.Collections.Generic;

namespace App.Common.Contracts
{
    public class HeaderResponseContract
    {
        public int Status { get; set; }
        public string Message { get; set; }
    }
}
