﻿using System;
using App.Common.Contracts;

namespace App.Common.Helpers
{
    public static class ResponseHelper
    {
        public static ResponseContract<T> AsSuccessResponse<T>(this T data){
            ResponseContract<T> response = new ResponseContract<T>();
            response.Header.Status = 200;
            response.Data = data;
            return response;
        }

        public static ResponseContract<T> AsErrorResponse<T>(this T entity, string error)
        {
            ResponseContract<T> response = new ResponseContract<T>();
            response.Header.Status = 404;
            response.Header.Message = error;
            response.Data = entity;
            return response;
        }
    }
}
