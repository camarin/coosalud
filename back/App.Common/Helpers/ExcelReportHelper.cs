﻿using OfficeOpenXml;
using System;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;

namespace App.Common.Helpers
{
    public class ExcelReportHelper
    {
        public string DataTableToExcel(DataTable table, string reportName)
        {
            string path = ConfigurationManager.AppSettings.Get("REPORSTPATH");
            Regex rgx = new Regex(@" *\s");// Ruta relativa donde se van a guardar los archivos
            string fileName = rgx.Replace(reportName, "_") + $"_{DateTime.Now.Ticks}";
            string fullPath = Path.Combine(HttpContext.Current.Server.MapPath(path), string.Format("{0}.xlsx", fileName));

            using (var package = new ExcelPackage(new FileInfo(fullPath)))
            {

                ExcelWorksheet ws = package.Workbook.Worksheets.Add(reportName);
                ws.Cells.Style.Font.Size = 12; //Default font size for whole sheet
                ws.Cells.Style.Font.Name = "Calibri"; //Default Font name for whole sheet
                
                // Cabecera o primera fila del reporte
                int columnIndex = 1;
                foreach (DataColumn column in table.Columns)
                {
                    ws.Cells[1, columnIndex].Value = column.ColumnName;
                    columnIndex++;
                }

                ws.Cells[1, 1, 1, columnIndex].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin, Color.Black);
                ws.Cells[1, 1, 1, columnIndex].Style.Font.Bold = true;

                //Cuerpo del reporte
                int rowIndex = 2;
                columnIndex = 1;
                foreach (DataRow row in table.Rows)
                {
                    foreach (DataColumn column in table.Columns)
                    {
                        ws.Cells[rowIndex, columnIndex].Value = row[column.ColumnName].ToString();
                        columnIndex++;
                    }
                    columnIndex = 1;
                    rowIndex++;
                }

                ws.Cells.AutoFitColumns();
                package.Save();
            }
            return string.Format("{0}.xlsx", fileName);
        }

    }
}
