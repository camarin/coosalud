﻿using System.ComponentModel.DataAnnotations;

namespace App.Common.Extensions
{
    public class CustomPropertiesInfo : ValidationAttribute
    {
        public string Name
        {
            get;
            set;
        }
        public bool InTable
        {
            get;
            set;
        }

        public bool IsForeign
        {
            get;
            set;
        
        }
        public string ForeignValue
        {
            get;
            set;
        }

        public string ForeignKey
        {
            get;
            set;
        }

        public string ForeignController
        {
            get;
            set;
        }

        public bool IsCheckBox
        {
            get;
            set;
        }

        public bool Required
        {
            get;
            set;
        }
        public bool PrimaryKey { get; set; }

        public CustomPropertiesInfo()
        {
            Name = string.Empty;
            InTable = false;
            IsForeign = false;
            ForeignKey = string.Empty;
            ForeignValue = string.Empty;
            ForeignController = string.Empty;
            IsCheckBox = false;
            Required = true;
            PrimaryKey = false;
        }

        public override bool IsValid(object value)
        {
            return true;
        }
    }
}
