using App.Repository.Def.POCOS.Sys;
using App.Repository.Def.Base;

namespace App.Repository.Def.Definitions.Sys
{
    public interface IDocumentTypeRepository : ISharedRepository<DocumentTypePOCO>
    {
    }
}
