using App.Repository.Def.POCOS.Inv;
using App.Repository.Def.Base;

namespace App.Repository.Def.Definitions.Inv
{
    public interface IInventoryStateRepository : ISharedRepository<InventoryStatePOCO>
    {
    }
}
