using App.Repository.Def.POCOS.Inv;
using App.Repository.Def.Base;
using System.Collections.Generic;

namespace App.Repository.Def.Definitions.Inv
{
    public interface IInventoryRepository : ISharedRepository<InventoryPOCO>
    {
        IEnumerable<VwInventoryPOCO> GetVwAll();
    }
}
