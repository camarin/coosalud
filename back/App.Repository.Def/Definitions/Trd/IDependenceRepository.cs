using App.Repository.Def.POCOS.Trd;
using App.Repository.Def.Base;
using System.Collections.Generic;

namespace App.Repository.Def.Definitions.Trd
{
    public interface IDependenceRepository : ISharedRepository<DependencePOCO>
    {
        IEnumerable<VwDependencePOCO> GetVwAll();
    }
}
