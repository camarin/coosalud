using App.Repository.Def.POCOS.Trd;
using App.Repository.Def.Base;

namespace App.Repository.Def.Definitions.Trd
{
    public interface IEntityRepository : ISharedRepository<EntityPOCO>
    {
    }
}
