using App.Repository.Def.POCOS.Auth;
using App.Repository.Def.Base;

namespace App.Repository.Def.Definitions.Auth
{
    public interface IRoleRepository : ISharedRepository<RolePOCO>
    {
    }
}
