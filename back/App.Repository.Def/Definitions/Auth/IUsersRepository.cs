using App.Repository.Def.POCOS.Auth;
using App.Repository.Def.Base;
using System.Collections.Generic;

namespace App.Repository.Def.Definitions.Auth
{
    public interface IUsersRepository : ISharedRepository<UsersPOCO>
    {
        void UpdateToken(string username,string token);
        IEnumerable<VwUsersPOCO> GetVwAll();
    }
}
