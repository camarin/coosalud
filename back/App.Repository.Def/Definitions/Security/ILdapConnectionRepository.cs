﻿namespace App.Repository.Def.Definitions.Security
{
    public interface  ILdapConnectionRepository
    {
        bool ValidateIfUserExist(string username, string password);
    }
}
