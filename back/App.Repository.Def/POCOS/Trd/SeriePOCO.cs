namespace App.Repository.Def.POCOS.Trd
{
    public class SeriePOCO
    {
        public int SerieId { get; set; }
        public int DependenceId { get; set; }
        public string Code { get; set; }
        public string SerieName { get; set; }
    }
}
