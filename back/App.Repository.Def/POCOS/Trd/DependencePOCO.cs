namespace App.Repository.Def.POCOS.Trd
{
    public class DependencePOCO
    {
        public int DependenceId { get; set; }
        public int EntityId { get; set; }
        public string Code { get; set; }
        public string DependenceName { get; set; }
    }
}
