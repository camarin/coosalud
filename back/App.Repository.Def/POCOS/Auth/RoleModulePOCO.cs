namespace App.Repository.Def.POCOS.Auth
{
    public class RoleModulePOCO
    {
        public int RoleModuleId { get; set; }
        public int RoleId { get; set; }
        public int ModuleId { get; set; }
    }
}
