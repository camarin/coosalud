namespace App.Repository.Def.POCOS.Auth
{
    public class RolePOCO
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
    }
}
