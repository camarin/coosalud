namespace App.Repository.Def.POCOS.Auth
{
    public class UsersPOCO
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public bool State { get; set; }
        public int RoleId { get; set; }
    }
}
