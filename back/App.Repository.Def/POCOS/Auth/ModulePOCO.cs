namespace App.Repository.Def.POCOS.Auth
{
    public class ModulePOCO
    {
        public int ModuleId { get; set; }
        public string ModuleName { get; set; }
    }
}
