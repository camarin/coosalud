namespace App.Repository.Def.POCOS.Inv
{
    public class InventoryStatePOCO
    {
        public int InventoryStateId { get; set; }
        public string InventoryStateName { get; set; }
    }
}
