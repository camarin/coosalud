﻿using System.Collections.Generic;

namespace App.Repository.Def.Base
{
    public interface ISharedRepository<TPOCO>
    {
        IEnumerable<TPOCO> All { get; }
        void Delete(object id);
        TPOCO FindById(int Id);
        void Insert(TPOCO entity);
        void Update(TPOCO entity, object id);
    }
}
