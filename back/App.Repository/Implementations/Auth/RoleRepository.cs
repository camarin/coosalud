using App.Model.Base;
using App.Model.Def.Base;
using App.Model.Def.Entities.Auth;
using App.Repository.Base;
using App.Repository.Def.POCOS.Auth;

namespace App.Repository.Implementations.Auth
{
    public class RoleRepository : BasePOCORepository<RoleEntity, RolePOCO>, Def.Definitions.Auth.IRoleRepository
    {
        public RoleRepository()
        : base(RoleProperties.Build())
        {
        }
    }



    class RoleProperties : BaseTableProperties<RoleEntity>
    {
        public override string Sheema => "[Auth]";
        public override string TableName => "[Role]";
        public override bool Autoincrement => true;
        public override string TableId => "RoleId";

        public static IBaseModel<RoleEntity> Build()
        {
            return new BaseModel<RoleEntity>(new RoleProperties());
        }
    }
}

