using App.Model.Base;
using App.Model.Def.Base;
using App.Model.Def.Entities.Auth;
using App.Repository.Base;
using App.Repository.Def.POCOS.Auth;
using System.Collections.Generic;

namespace App.Repository.Implementations.Auth
{
    public class UsersRepository : BasePOCORepository<UsersEntity, UsersPOCO>, Def.Definitions.Auth.IUsersRepository
    {
        public UsersRepository()
        : base(UsersProperties.Build())
        {
        }

        public IEnumerable<VwUsersPOCO> GetVwAll()
        {
            return _entities.Execute<VwUsersPOCO>("SELECT * FROM [Auth].[VwUser]");
        }

        public void UpdateToken(string username, string token)
        {
            
            Dictionary<string, object> parameters = new Dictionary<string, object>()
            {
                { "SessionHash" , token },
                { "UserName" , username },
            };
            _entities.ExecuteNonQuery("UPDATE [Users].[Users] SET SessionHash = @SessionHash WHERE UserName = @UserName", parameters);
        }
    }



    class UsersProperties : BaseTableProperties<UsersEntity>
    {
        public override string Sheema => "[Auth]";
        public override string TableName => "[User]";
        public override string TableId => "UserId";
        public override bool Autoincrement => true;

        public static IBaseModel<UsersEntity> Build()
        {
            return new BaseModel<UsersEntity>(new UsersProperties());
        }
    }
}

