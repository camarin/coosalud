using App.Model.Base;
using App.Model.Def.Base;
using App.Model.Def.Entities.Auth;
using App.Repository.Base;
using App.Repository.Def.POCOS.Auth;

namespace App.Repository.Implementations.Auth
{
    public class RoleModuleRepository : BasePOCORepository<RoleModuleEntity, RoleModulePOCO>, Def.Definitions.Auth.IRoleModuleRepository
    {
        public RoleModuleRepository()
        : base(RoleModuleProperties.Build())
        {
        }
    }



    class RoleModuleProperties : BaseTableProperties<RoleModuleEntity>
    {
        public override string Sheema => "Auth";
        public override string TableName => "RoleModule";
        public override string TableId => "RoleModuleId";

        public static IBaseModel<RoleModuleEntity> Build()
        {
            return new BaseModel<RoleModuleEntity>(new RoleModuleProperties());
        }
    }
}

