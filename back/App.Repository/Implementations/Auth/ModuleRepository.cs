using App.Model.Base;
using App.Model.Def.Base;
using App.Model.Def.Entities.Auth;
using App.Repository.Base;
using App.Repository.Def.POCOS.Auth;

namespace App.Repository.Implementations.Auth
{
    public class ModuleRepository : BasePOCORepository<ModuleEntity, ModulePOCO>, Def.Definitions.Auth.IModuleRepository
    {
        public ModuleRepository()
        : base(ModuleProperties.Build())
        {
        }
    }



    class ModuleProperties : BaseTableProperties<ModuleEntity>
    {
        public override string Sheema => "Auth";
        public override string TableName => "Module";
        public override string TableId => "ModuleId";

        public static IBaseModel<ModuleEntity> Build()
        {
            return new BaseModel<ModuleEntity>(new ModuleProperties());
        }
    }
}

