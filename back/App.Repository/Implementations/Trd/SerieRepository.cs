using App.Model.Base;
using App.Model.Def.Base;
using App.Model.Def.Entities.Trd;
using App.Repository.Base;
using App.Repository.Def.POCOS.Trd;

namespace App.Repository.Implementations.Trd
{
    public class SerieRepository : BasePOCORepository<SerieEntity, SeriePOCO>, Def.Definitions.Trd.ISerieRepository
    {
        public SerieRepository()
        : base(SerieProperties.Build())
        {
        }
    }



    class SerieProperties : BaseTableProperties<SerieEntity>
    {
        public override string Sheema => "Trd";
        public override string TableName => "Serie";
        public override string TableId => "SerieId";

        public static IBaseModel<SerieEntity> Build()
        {
            return new BaseModel<SerieEntity>(new SerieProperties());
        }
    }
}

