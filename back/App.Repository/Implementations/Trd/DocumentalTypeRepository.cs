using App.Model.Base;
using App.Model.Def.Base;
using App.Model.Def.Entities.Trd;
using App.Repository.Base;
using App.Repository.Def.POCOS.Trd;

namespace App.Repository.Implementations.Trd
{
    public class DocumentalTypeRepository : BasePOCORepository<DocumentalTypeEntity, DocumentalTypePOCO>, Def.Definitions.Trd.IDocumentalTypeRepository
    {
        public DocumentalTypeRepository()
        : base(DocumentalTypeProperties.Build())
        {
        }
    }



    class DocumentalTypeProperties : BaseTableProperties<DocumentalTypeEntity>
    {
        public override string Sheema => "Trd";
        public override string TableName => "DocumentalType";
        public override string TableId => "DocumentalTypeId";

        public static IBaseModel<DocumentalTypeEntity> Build()
        {
            return new BaseModel<DocumentalTypeEntity>(new DocumentalTypeProperties());
        }
    }
}

