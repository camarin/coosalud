using System.Collections.Generic;
using App.Model.Base;
using App.Model.Def.Base;
using App.Model.Def.Entities.Trd;
using App.Repository.Base;
using App.Repository.Def.POCOS.Trd;

namespace App.Repository.Implementations.Trd
{
    public class DependenceRepository : BasePOCORepository<DependenceEntity, DependencePOCO>, Def.Definitions.Trd.IDependenceRepository
    {
        public DependenceRepository()
        : base(DependenceProperties.Build())
        {
        }

        public IEnumerable<VwDependencePOCO> GetVwAll()
        {
            return _entities.Execute<VwDependencePOCO>("SELECT * FROM [Trd].[VwDependence]");
        }
    }



    class DependenceProperties : BaseTableProperties<DependenceEntity>
    {
        public override string Sheema => "Trd";
        public override string TableName => "Dependence";
        public override string TableId => "DependenceId";

        public static IBaseModel<DependenceEntity> Build()
        {
            return new BaseModel<DependenceEntity>(new DependenceProperties());
        }
    }
}

