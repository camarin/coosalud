using App.Model.Base;
using App.Model.Def.Base;
using App.Model.Def.Entities.Trd;
using App.Repository.Base;
using App.Repository.Def.POCOS.Trd;

namespace App.Repository.Implementations.Trd
{
    public class EntityRepository : BasePOCORepository<EntityEntity, EntityPOCO>, Def.Definitions.Trd.IEntityRepository
    {
        public EntityRepository()
        : base(EntityProperties.Build())
        {
        }
    }



    class EntityProperties : BaseTableProperties<EntityEntity>
    {
        public override string Sheema => "Trd";
        public override string TableName => "Entity";
        public override string TableId => "EntityId";

        public static IBaseModel<EntityEntity> Build()
        {
            return new BaseModel<EntityEntity>(new EntityProperties());
        }
    }
}

