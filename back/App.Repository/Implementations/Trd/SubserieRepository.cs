using App.Model.Base;
using App.Model.Def.Base;
using App.Model.Def.Entities.Trd;
using App.Repository.Base;
using App.Repository.Def.POCOS.Trd;

namespace App.Repository.Implementations.Trd
{
    public class SubserieRepository : BasePOCORepository<SubserieEntity, SubseriePOCO>, Def.Definitions.Trd.ISubserieRepository
    {
        public SubserieRepository()
        : base(SubserieProperties.Build())
        {
        }
    }



    class SubserieProperties : BaseTableProperties<SubserieEntity>
    {
        public override string Sheema => "Trd";
        public override string TableName => "Subserie";
        public override string TableId => "SubserieId";

        public static IBaseModel<SubserieEntity> Build()
        {
            return new BaseModel<SubserieEntity>(new SubserieProperties());
        }
    }
}

