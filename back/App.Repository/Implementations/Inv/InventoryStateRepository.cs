using App.Model.Base;
using App.Model.Def.Base;
using App.Model.Def.Entities.Inv;
using App.Repository.Base;
using App.Repository.Def.POCOS.Inv;

namespace App.Repository.Implementations.Inv
{
    public class InventoryStateRepository : BasePOCORepository<InventoryStateEntity, InventoryStatePOCO>, Def.Definitions.Inv.IInventoryStateRepository
    {
        public InventoryStateRepository()
        : base(InventoryStateProperties.Build())
        {
        }
    }



    class InventoryStateProperties : BaseTableProperties<InventoryStateEntity>
    {
        public override string Sheema => "Inv";
        public override string TableName => "InventoryState";

        public static IBaseModel<InventoryStateEntity> Build()
        {
            return new BaseModel<InventoryStateEntity>(new InventoryStateProperties());
        }
    }
}

