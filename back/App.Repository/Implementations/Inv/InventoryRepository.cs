using App.Model.Base;
using App.Model.Def.Base;
using App.Model.Def.Entities.Inv;
using App.Repository.Base;
using App.Repository.Def.POCOS.Inv;
using System.Collections.Generic;

namespace App.Repository.Implementations.Inv
{
    public class InventoryRepository : BasePOCORepository<InventoryEntity, InventoryPOCO>, Def.Definitions.Inv.IInventoryRepository
    {
        public InventoryRepository()
        : base(InventoryProperties.Build())
        {
        }

        public IEnumerable<VwInventoryPOCO> GetVwAll()
        {
            return _entities.Execute<VwInventoryPOCO>("SELECT * FROM [Inv].[VwInventory]");
        }
    }



    class InventoryProperties : BaseTableProperties<InventoryEntity>
    {
        public override string Sheema => "Inv";
        public override string TableName => "Inventory";

        public static IBaseModel<InventoryEntity> Build()
        {
            return new BaseModel<InventoryEntity>(new InventoryProperties());
        }
    }
}

