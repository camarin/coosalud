﻿using App.Repository.Def.Definitions.Security;
using System;
using System.Configuration;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices.Protocols;

namespace App.Repository.Implementations.Security
{
    public class LdapConnectionRepository : ILdapConnectionRepository
    {
        public bool ValidateIfUserExist(string username, string password)
        {
            string domain = ConfigurationManager.AppSettings.Get("LDAPHOST");
            try
            {
                Exception ex = new Exception("User or password is invalid");
                using (var pc = new PrincipalContext(ContextType.Domain, domain, null, username, password))
                {
                    using (var foundUser = UserPrincipal.FindByIdentity(pc, IdentityType.SamAccountName, username))
                    {
                        if (foundUser == null)
                        {
                            throw ex;
                        }

                        return true;
                    }
                }
            }
            catch (LdapException ex)
            {
                //XpertGroup.Core.Logger.Logger.Current.Error($"(LDAPProviderModel) LdapException: {ex.Message}");

                switch (ex.ErrorCode)
                {
                    case 49:
                        throw new Exception("Usuario o contraseña incorrecta");
                    case 81:
                        throw new Exception("No fue posible conectarse coon el directorio activo.");
                    default:
                        throw new Exception("Ha ocurrido un error inesperado.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
