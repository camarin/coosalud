using App.Model.Base;
using App.Model.Def.Base;
using App.Model.Def.Entities.Sys;
using App.Repository.Base;
using App.Repository.Def.POCOS.Sys;

namespace App.Repository.Implementations.Sys
{
    public class DocumentTypeRepository : BasePOCORepository<DocumentTypeEntity, DocumentTypePOCO>, Def.Definitions.Sys.IDocumentTypeRepository
    {
        public DocumentTypeRepository()
        : base(DocumentTypeProperties.Build())
        {
        }
    }



    class DocumentTypeProperties : BaseTableProperties<DocumentTypeEntity>
    {
        public override string Sheema => "Sys";
        public override string TableName => "DocumentType";

        public static IBaseModel<DocumentTypeEntity> Build()
        {
            return new BaseModel<DocumentTypeEntity>(new DocumentTypeProperties());
        }
    }
}

