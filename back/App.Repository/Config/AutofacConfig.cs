﻿using App.Repository.Implementations.Auth;
using Autofac;

namespace App.Repository.Config
{
    public static class AutofacConfig
    {
        public static void RegisterTypes(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(UsersRepository).Assembly)
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces().InstancePerRequest();

            Model.Config.AutofacConfig.RegisterTypes(builder);
        }
    }
}
