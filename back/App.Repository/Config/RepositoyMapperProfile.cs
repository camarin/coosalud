﻿using App.Model.Def.Entities.Auth;
using App.Repository.Def.POCOS.Auth;
using AutoMapper;

namespace App.Repository.Config
{
    public class RepositoyMapperProfile : Profile
    {
        public RepositoyMapperProfile()
        {
            CreateMap<UsersEntity, UsersPOCO>();
            CreateMap<RoleEntity, RolePOCO>();
        }
    }
}
