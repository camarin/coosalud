﻿using App.Model.Def.Base;
using App.Repository.Def.Base;
using AutoMapper;
using System;
using System.Collections.Generic;

namespace App.Repository.Base
{
    public class BasePOCORepository<TEntity, TPOCO> : ISharedRepository<TPOCO>
        where TEntity : class
        where TPOCO : class, new()
    {

        protected IBaseModel<TEntity> _entities;

        protected BasePOCORepository(IBaseModel<TEntity> entities)
        {
            _entities = entities ?? throw new ArgumentNullException(nameof(entities));
        }

        public virtual IEnumerable<TPOCO> All
        {
            get
            {
                return Mapper.Map<IEnumerable<TPOCO>>(_entities.All());
            }
        }

        public virtual void Insert(TPOCO entity)
        {
            _entities.Insert(Mapper.Map<TEntity>(entity));
        }

        public virtual void Delete(object id)
        {
            _entities.Delete(id);
        }

        public virtual void Update(TPOCO entity, object id)
        {
            _entities.Update(Mapper.Map<TEntity>(entity), id);
        }

        public virtual TPOCO FindById(int id)
        {
            return Mapper.Map<TPOCO>(_entities.FindById(id)) ;
        }
    }
}

