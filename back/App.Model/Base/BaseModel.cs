﻿using App.Model.Def.Base;
using App.Model.lib;
using System.Collections.Generic;
using System.Data;

namespace App.Model.Base
{
    public class BaseModel<TEntity> : IBaseModel<TEntity>
        where TEntity : class
    {

        protected DataAcces dataAcces;
        protected BaseTableProperties<TEntity> _props;
        protected QueryHelper<TEntity> _queryHelper;

        public BaseModel(BaseTableProperties<TEntity> props)
        {
            _props = props;
            dataAcces = new DataAcces();
            _queryHelper = new QueryHelper<TEntity>(_props);
        }

        public IEnumerable<TEntity> All()
        {
            return dataAcces.Execute<TEntity>(_queryHelper.PerformSelect());
        }

        public void Delete(object id)
        {
            dataAcces.ExecuteNonQuery(_queryHelper.PerformDelete(id));
        }

        public IEnumerable<T> Execute<T>(string query)
        {
            return dataAcces.Execute<T>(query);
        }

        public IEnumerable<T> Execute<T>(string query, Dictionary<string, object> parameters)
        {
            return dataAcces.Execute<T>(query, parameters);
        }

        public void ExecuteNonQuery(string query)
        {
            dataAcces.ExecuteNonQuery(query);
        }

        public DataTable ExecuteQuery(string query)
        {
            return dataAcces.ExecuteQuery(query);
        }

        public void ExecuteNonQuery(string query, Dictionary<string, object> parameters)
        {
            dataAcces.ExecuteNonQuery(query, parameters);
        }

        public TEntity FindById(int id)
        {
            throw new System.NotImplementedException();
        }

        public void Insert(TEntity entity)
        {
            dataAcces.ExecuteNonQuery(_queryHelper.PerformInsert(entity), _queryHelper.GetEntityValues(entity));
        }

        public void Update(TEntity entity, object id)
        {
            dataAcces.ExecuteNonQuery(_queryHelper.PerformUpdate(entity, id), _queryHelper.GetEntityValues(entity, false));
        }
    }
}
