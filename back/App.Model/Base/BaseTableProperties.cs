﻿namespace App.Model.Base
{
    public  class BaseTableProperties<TEntity>
        where TEntity : class
    {
        public virtual string Sheema { get; set; }
        public virtual string TableName { get { return $"{nameof(TEntity)}"; }}
        public virtual string TableId { get { return $"{TableName}Id"; }}
        public virtual bool Autoincrement { get { return true; } }
    }
}
