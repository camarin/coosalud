﻿using App.Model.Base;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace App.Model.lib
{
    public class QueryHelper<TEntity>
        where TEntity : class
    {
        protected const string SELECT = "SELECT ";
        protected const string INSERT = "INSERT ";
        protected const string DELETE = "DELETE ";
        protected const string UPDATE = "UPDATE ";
        protected const string SET = " SET ";
        protected const string INTO = " INTO ";
        protected const string FROM = " FROM ";
        protected const string WHERE = " WHERE ";
        protected const string OR = " OR ";
        protected const string AND = " AND ";
        protected const string VALUES = " VALUES ";
        protected BaseTableProperties<TEntity> _props;

        public QueryHelper(BaseTableProperties<TEntity> props)
        {
            _props = props;
        }

        public string PerformSelect()
        {
            StringBuilder command = new StringBuilder();
            command.Append(SELECT);


            Type type = typeof(TEntity);
            IEnumerable<PropertyInfo> props = type.GetProperties();
            List<string> keys = new List<string>();

            foreach (var prop in props)
            {
                keys.Add(prop.Name);
            }

            command.Append(String.Join(",", keys));
            command.Append(FROM);
            command.Append($"{_props.Sheema}.{_props.TableName}");

            return command.ToString();
        }

        public string PerformInsert(TEntity entity)
        {
            StringBuilder command = new StringBuilder();
            command.Append(INSERT);
            command.Append(INTO);
            command.Append($"{_props.Sheema}.{_props.TableName}");
            command.Append("({0})");
            command.Append(VALUES + "({1})");

            Type type = typeof(TEntity);
            IEnumerable<PropertyInfo> props = type.GetProperties();
            List<string> keys = new List<string>();
            List<string> values = new List<string>();

            foreach (var prop in props)
            {
                if (prop.Name == _props.TableId && _props.Autoincrement)
                {
                    continue;
                }
                keys.Add(prop.Name);
                values.Add($"@{prop.Name}");
            }


            //throw new Exception(string.Format(command.ToString(), string.Join(",", keys), string.Join(",", values)));
            return string.Format(command.ToString(), string.Join(",", keys), string.Join(",", values));
        }

        public Dictionary<string, object> GetEntityValues(TEntity entity, bool insert = true)
        {
            Type type = typeof(TEntity);
            IEnumerable<PropertyInfo> props = type.GetProperties();

            Dictionary<string, object> response = new Dictionary<string, object>();

            foreach (var prop in props)
            {
                if (prop.Name == _props.TableId && _props.Autoincrement && insert)
                {
                    continue;
                }
                response.Add(prop.Name, entity.GetType().GetProperty(prop.Name).GetValue(entity, null));
            }
            return response;
        }

        public string PerformDelete(object id)
        {
            StringBuilder command = new StringBuilder();
            command.Append(DELETE);
            command.Append(FROM);
            command.Append($"{_props.Sheema}.{_props.TableName}");
            command.Append(WHERE);
            command.Append("{0}");

            string parsedId = "";
            Type keyType = id.GetType();
            if (keyType == typeof(string))
            {
                parsedId = $"'{id}'";
            }
            else
            {
                parsedId = $"{id}";
            }
            string where = $"{_props.TableId} = {parsedId}";

            return string.Format(command.ToString(), where);
        }

        //public string PerformUpdate(TEntity entity, object id)
        //{
        //    StringBuilder command = new StringBuilder();
        //    command.Append(UPDATE);
        //    command.Append($"{_props.Sheema}.{_props.TableName}");
        //    command.Append(SET);
        //    command.Append("{0} {1}");

        //    Type type = typeof(TEntity);
        //    IEnumerable<PropertyInfo> props = type.GetProperties();
        //    List<string> parsedValues = new List<string>();

        //    foreach (var prop in props)
        //    {

        //        if (prop.Name == _props.TableId && _props.Autoincrement)
        //        {
        //            continue;
        //        }
        //        string parsedValue = "";
        //        object value = entity.GetType().GetProperty(prop.Name).GetValue(entity, null);

        //        if (value != null)
        //        {
        //            Type valueType = value.GetType();
        //            if (valueType == typeof(string))
        //            {
        //                parsedValue = $"'{value}'";
        //            }
        //            else if (valueType == typeof(bool))
        //            {
        //                int boolValue = Convert.ToBoolean(value) ? 1 : 0;
        //                parsedValue = $"{boolValue}";
        //            }
        //            else if (valueType == typeof(int) || valueType == typeof(decimal) || valueType == typeof(float) || valueType == typeof(double))
        //            {
        //                parsedValue = $"{value}";
        //            }
        //        }
        //        else
        //        {
        //            parsedValue = "";
        //        }

        //        parsedValues.Add($"{prop.Name} = {parsedValue}");
        //    }

        //    string parsedId = "";
        //    Type keyType = id.GetType();
        //    if (keyType == typeof(string))
        //    {
        //        parsedId = $"'{id}'";
        //    }
        //    else
        //    {
        //        parsedId = $"{id}";
        //    }
        //    string where = $"{WHERE} {_props.TableId} = {parsedId}";

        //    return string.Format(command.ToString(), string.Join(",", parsedValues), where);
        //}

        public string PerformUpdate(TEntity entity, object id)
        {
            StringBuilder command = new StringBuilder();
            command.Append(UPDATE);
            command.Append($"{_props.Sheema}.{_props.TableName}");
            command.Append(SET);
            command.Append("{0} {1}");

            Type type = typeof(TEntity);
            IEnumerable<PropertyInfo> props = type.GetProperties();
            List<string> parsedValues = new List<string>();

            foreach (var prop in props)
            {

                if (prop.Name == _props.TableId && _props.Autoincrement)
                {
                    continue;
                }

                parsedValues.Add($"{prop.Name} = @{prop.Name}");
            }
            string where = $"{WHERE} {_props.TableId} = @{_props.TableId}";

            return string.Format(command.ToString(), string.Join(",", parsedValues), where);
        }
    }
}
