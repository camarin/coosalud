﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using AutoMapper;
namespace App.Model.lib
{
    public class DataAcces
    {
        private string connectionString;
        public DataAcces()
        {
            connectionString = ConfigurationManager.ConnectionStrings["connection"].ConnectionString;
        }

        /// <summary>
        /// Ejecuta una sentencia sin parametros que retorna un resultado
        /// </summary>
        /// <returns>Coleccion del tipo especificado.</returns>
        /// <param name="query">Query.</param>
        /// <typeparam name="T">Tipo de entidad.</typeparam>
        public IEnumerable<T> Execute<T>(string query)
        {
            using (var connection = new SqlConnection(connectionString))
            using (var command = new SqlCommand(query, connection))
            {
                connection.Open();
                using (var reader = command.ExecuteReader())
                    if (reader.HasRows)
                        return Mapper.Map<IDataReader, IEnumerable<T>>(reader);
            }
            return new List<T>();
        }

        /// <summary>
        /// Ejecuta una sentencia con parametros que retorna un resultado
        /// </summary>
        /// <returns>Coleccion del tipo especificado.</returns>
        /// <param name="query">Query.</param>
        /// <param name="parameters">Parameters.</param>
        /// <typeparam name="T">Tipo de entidad.</typeparam>
        // @"UPDATE Customer SET Name = @NewName, Age = @NewAge WHERE ID = @RecordID";
        public IEnumerable<T> Execute<T>(string query, Dictionary<string, object> parameters)
        {
            using (var connection = new SqlConnection(connectionString))
            using (var command = new SqlCommand(query, connection))
            {
                foreach (var param in parameters)
                {
                    command.Parameters.Add(new SqlParameter(param.Key, param.Value ?? DBNull.Value));
                }
                connection.Open();
                using (var reader = command.ExecuteReader())
                    if (reader.HasRows)
                        return Mapper.Map<IDataReader, IEnumerable<T>>(reader);
            }
            return null;
        }

        /// <summary>
        /// Ejecuta una sentencia sin parametros
        /// </summary>
        /// <param name="query">Query.</param>
        public void ExecuteNonQuery(string query)
        {
            using (var connection = new SqlConnection(connectionString))
            using (var command = new SqlCommand(query, connection))
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Ejecuta una sentencia con parametros
        /// </summary>
        /// <param name="query">Query.</param>
        /// <param name="parameters">Parameters.</param>
        public void ExecuteNonQuery(string query, Dictionary<string, object> parameters)
        {
            using (var connection = new SqlConnection(connectionString))
            using (var command = new SqlCommand(query, connection))
            {
                connection.Open();

                foreach (var param in parameters)
                {
                    command.Parameters.Add(new SqlParameter(param.Key, param.Value ?? DBNull.Value));
                }

                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Ejecuta una sentencia y retorna un DataTable
        /// </summary>
        /// <param name="query">Query.</param>
        public DataTable ExecuteQuery(string query)
        {
            DataTable response = new DataTable();
            using (var connection = new SqlConnection(connectionString))
            using (var command = new SqlCommand(query, connection))
            {
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    response.Load(reader);
                }
            }
            return response;
        }
    }
}
