﻿using App.Model.Def.Base;
using App.Model.Def.Entities.Auth;
using Autofac;

namespace App.Model.Config
{
    public static class AutofacConfig
    {
        public static void RegisterTypes(ContainerBuilder builder)
        {
            builder.RegisterType<Base.BaseModel<UsersEntity>>().As<IBaseModel<UsersEntity>>();
        }
    }
}
