﻿using System.Collections.Generic;
using System.Web.Http.Cors;
using App.Common.Contracts;
using App.Domain.Def.Base;
using App.Common.Helpers;
using System.Web.Http;
using System;
using codeRR.Client;

namespace App.Api.Base
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class BaseApiController<TDTO> : ApiController
        where TDTO : class, new()
    {
        readonly IBaseSharedService<TDTO> _service;
        public BaseApiController(IBaseSharedService<TDTO> service)
        {
            _service = service;
        }

        // GET api/<controller>/GetAll
        public ResponseContract<IEnumerable<TDTO>> GetAll()
        {

            IEnumerable<TDTO> response = new List<TDTO>();
            try
            {
                return _service.GetAll().AsSuccessResponse();
            }
            catch (Exception ex)
            {
                Err.Report(ex);
                return response.AsErrorResponse(ex.Message);
            }

        }

        // POST api/<controller>/Create
        [HttpPost]
        public ResponseContract<bool> Create(TDTO request)
        {
            try
            {
                _service.Create(request);
                return true.AsSuccessResponse();

            }
            catch (Exception ex)
            {
                //Err.Report(ex, request);
                return false.AsErrorResponse(ex.Message);
            }
        }

        // POST api/<controller>/Edit
        [HttpPost]
        public ResponseContract<bool> Edit(UpdateRequestContract<TDTO> request)
        {
            try
            {
                _service.Update(request.Id,request.Data);
                return true.AsSuccessResponse();
            }
            catch (Exception ex)
            {
                return false.AsErrorResponse(ex.Message);
            }
        }

        // POST api/<controller>/Delete
        [HttpPost]
        public ResponseContract<bool> Delete(DeleteRequestContract request)
        {
            try
            {
                _service.Delete(request.Id);
                return true.AsSuccessResponse();
            }
            catch (Exception ex)
            {
                return false.AsErrorResponse(ex.Message);
            }
        }

        public ResponseContract<object> GetProperties()
        {
            object response = new { };
            try
            {

                return _service.GetProperties().AsSuccessResponse();
            }
            catch (Exception ex)
            {
                return response.AsErrorResponse(ex.Message);
            }
        }

    }
}
