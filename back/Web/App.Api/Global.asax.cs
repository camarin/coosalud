﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Http;
using System;
using codeRR.Client;

namespace App.Api
{
    public class Global : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            App.Api.App_Start.AutofacConfig.RegisterTypes();
            App.Domain.Config.AutoMapperConfig.Configure();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            //replace with your server URL and your appkey/SharedSecret.
            var url = new Uri("http://192.168.56.101:8002/");
            Err.Configuration.Credentials(url,
                                          "9d6d85e97ff44939854f00df87ba6476",
                                          "f5a3555f21a44f6b94445af676b8d350");
            Err.Configuration.CatchAspNetExceptions();
        }
    }
}
