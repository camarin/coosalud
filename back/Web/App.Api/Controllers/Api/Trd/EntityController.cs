using App.Domain.Def.Definitions.Trd;
using App.Domain.Def.DTO.Trd;
using System.Web.Http.Cors;
using App.Api.Base;

namespace App.Api.Controllers.Api.Trd
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class EntityController : BaseApiController<EntityDTO>
    {

        IEntityService _service;
        public EntityController(IEntityService service)
            :base(service)
        {
            _service = service;
        }
    }
}