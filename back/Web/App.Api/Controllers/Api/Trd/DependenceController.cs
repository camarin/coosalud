using App.Domain.Def.Definitions.Trd;
using App.Domain.Def.DTO.Trd;
using System.Web.Http.Cors;
using App.Api.Base;
using App.Common.Contracts;
using System.Web.Http;
using System.Collections.Generic;
using System;
using App.Common.Helpers;

namespace App.Api.Controllers.Api.Trd
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DependenceController : BaseApiController<DependenceDTO>
    {

        IDependenceService _service;
        public DependenceController(IDependenceService service)
            :base(service)
        {
            _service = service;
        }


        [HttpGet]
        public ResponseContract<IEnumerable<VwDependenceDTO>> GetVwAll()
        {
            IEnumerable<VwDependenceDTO> response = new List<VwDependenceDTO>();
            try
            {
                return _service.GetVwAll().AsSuccessResponse();
            }
            catch (Exception ex)
            {
                return response.AsErrorResponse(ex.Message);
            }

        }
    }
}