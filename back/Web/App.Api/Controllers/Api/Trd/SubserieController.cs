using App.Domain.Def.Definitions.Trd;
using App.Domain.Def.DTO.Trd;
using System.Web.Http.Cors;
using App.Api.Base;

namespace App.Api.Controllers.Api.Trd
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SubserieController : BaseApiController<SubserieDTO>
    {

        ISubserieService _service;
        public SubserieController(ISubserieService service)
            :base(service)
        {
            _service = service;
        }
    }
}