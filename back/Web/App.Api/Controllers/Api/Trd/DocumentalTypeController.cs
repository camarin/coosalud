using App.Domain.Def.Definitions.Trd;
using App.Domain.Def.DTO.Trd;
using System.Web.Http.Cors;
using App.Api.Base;

namespace App.Api.Controllers.Api.Trd
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DocumentalTypeController : BaseApiController<DocumentalTypeDTO>
    {

        IDocumentalTypeService _service;
        public DocumentalTypeController(IDocumentalTypeService service)
            :base(service)
        {
            _service = service;
        }
    }
}