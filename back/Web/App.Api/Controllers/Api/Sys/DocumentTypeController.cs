using App.Domain.Def.Definitions.Sys;
using App.Domain.Def.DTO.Sys;
using System.Web.Http.Cors;
using App.Api.Base;

namespace App.Api.Controllers.Api.Sys
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DocumentTypeController : BaseApiController<DocumentTypeDTO>
    {

        IDocumentTypeService _service;
        public DocumentTypeController(IDocumentTypeService service)
            :base(service)
        {
            _service = service;
        }
    }
}