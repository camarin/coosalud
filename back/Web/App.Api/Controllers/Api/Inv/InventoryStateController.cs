using App.Domain.Def.Definitions.Inv;
using App.Domain.Def.DTO.Inv;
using System.Web.Http.Cors;
using App.Api.Base;

namespace App.Api.Controllers.Api.Inv
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class InventoryStateController : BaseApiController<InventoryStateDTO>
    {

        IInventoryStateService _service;
        public InventoryStateController(IInventoryStateService service)
            :base(service)
        {
            _service = service;
        }
    }
}