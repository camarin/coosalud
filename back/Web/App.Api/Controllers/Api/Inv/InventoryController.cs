using App.Api.Base;
using App.Common.Contracts;
using App.Common.Helpers;
using App.Domain.Def.Definitions.Inv;
using App.Domain.Def.DTO.Inv;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;

namespace App.Api.Controllers.Api.Inv
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class InventoryController : BaseApiController<InventoryDTO>
    {

        IInventoryService _service;
        public InventoryController(IInventoryService service)
            : base(service)
        {
            _service = service;
        }

        [HttpGet]
        public ResponseContract<IEnumerable<VwInventoryDTO>> GetVwAll()
        {
            IEnumerable<VwInventoryDTO> response = new List<VwInventoryDTO>();
            try
            {
                return _service.GetVwAll().AsSuccessResponse();
            }
            catch (Exception ex)
            {
                return response.AsErrorResponse(ex.Message);
            }

        }
    }
}