using App.Domain.Def.Definitions.Auth;
using App.Domain.Def.DTO.Auth;
using System.Web.Http.Cors;
using App.Api.Base;

namespace App.Api.Controllers.Api.Auth
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RoleModuleController : BaseApiController<RoleModuleDTO>
    {

        IRoleModuleService _service;
        public RoleModuleController(IRoleModuleService service)
            :base(service)
        {
            _service = service;
        }
    }
}