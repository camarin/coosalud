using App.Domain.Def.Definitions.Auth;
using App.Domain.Def.DTO.Auth;
using System.Web.Http.Cors;
using App.Api.Base;

namespace App.Api.Controllers.Api.Auth
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ModuleController : BaseApiController<ModuleDTO>
    {

        IModuleService _service;
        public ModuleController(IModuleService service)
            :base(service)
        {
            _service = service;
        }
    }
}