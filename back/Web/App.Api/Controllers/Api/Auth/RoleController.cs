using App.Api.Base;
using App.Common.Contracts;
using App.Common.Helpers;
using App.Domain.Def.Contracts.Auth;
using App.Domain.Def.Definitions.Auth;
using App.Domain.Def.DTO.Auth;
using System;
using System.Web.Http;
using System.Web.Http.Cors;

namespace App.Api.Controllers.Api.Auth
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RoleController : BaseApiController<RoleDTO>
    {

        IRoleService _service;
        public RoleController(IRoleService service)
            : base(service)
        {
            _service = service;
        }



        [HttpPost]
        public ResponseContract<bool> EditWithModules(RoleModulesRequestContract request)
        {
            try
            {
                _service.EditWithModules(request);
                return true.AsSuccessResponse();

            }
            catch (Exception ex)
            {
                return false.AsErrorResponse(ex.Message);
            }
        }
    }
}