using App.Api.Base;
using App.Common.Contracts;
using App.Common.Helpers;
using App.Domain.Def.Contracts.Users;
using App.Domain.Def.Definitions.Auth;
using App.Domain.Def.DTO.Auth;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;

namespace App.Api.Controllers.Api.Auth
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UsersController : BaseApiController<UsersDTO>
    {
        IUsersService _service;
        public UsersController(IUsersService service)
            : base(service)
        {
            _service = service;
        }


        [HttpPost]
        public ResponseContract<LoginUserResponseContract> Login(LoginUserRequestContract request)
        {
            LoginUserResponseContract response = new LoginUserResponseContract();
            try
            {
                return _service.Login(request).AsSuccessResponse();
            }
            catch (Exception ex)
            {
                return response.AsErrorResponse(ex.Message);
            }

        }

        [HttpGet]
        public ResponseContract<IEnumerable<VwUsersDTO>> GetVwAll()
        {
            IEnumerable<VwUsersDTO> response = new List<VwUsersDTO>();
            try
            {
                return _service.GetVwAll().AsSuccessResponse();
            }
            catch (Exception ex)
            {
                return response.AsErrorResponse(ex.Message);
            }

        }

        [HttpPost]
        public ResponseContract<bool> RequestPermissions(PermissionsRequestContract request)
        {
            try
            {
                return _service.RequestPermissions(request).AsSuccessResponse();
            }
            catch (Exception ex)
            {
                return false.AsErrorResponse(ex.Message);
            }

        }
    }
}