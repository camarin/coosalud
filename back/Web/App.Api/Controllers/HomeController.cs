﻿using System;
using System.Web.Mvc;

namespace App.Api.Controllers
{
    public class HomeController : Controller
    {
        public HomeController()
        {
        }

        public ActionResult Index()
        {
            return Json(new { }, JsonRequestBehavior.AllowGet);
        }
    }
}
