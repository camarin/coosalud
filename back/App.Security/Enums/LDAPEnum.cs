﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.Security.Enums
{
    public enum LDAPEnum
    {
        OK = 200,
        VALIDATIONERROR = 400,
        ERROR = 500
    }
}