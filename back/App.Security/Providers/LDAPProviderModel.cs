﻿using App.Security.Entities;
using App.Security.Enums;
using System;
using System.Collections;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices.Protocols;
using System.Net;
using System.Linq;
using doct.Localization;
using App.Common.Localization;

namespace App.Security.Providers
{
    public class LDAPProviderModel
    {
        private string Domain { get; set; }
        private string AdminGroup { get; set; }
        private string TecGroup { get; set; }
        private string AseGroup { get; set; }

        public LDAPProviderModel()
        {
            Domain = doct.Helper.WebConfigurationHelper.GetProperty("connectionStringName");
            AdminGroup = doct.Helper.WebConfigurationHelper.GetProperty("administradores");
            TecGroup = doct.Helper.WebConfigurationHelper.GetProperty("asesores");
            AseGroup = doct.Helper.WebConfigurationHelper.GetProperty("tecnico");
        }

        private ILocalizationManager _localizator;

        public ILocalizationManager Localizator
        {
            get
            {
                return _localizator ?? (_localizator = new LocalizationManager());
            }

        }

        public LDAPResponseEntity<LDAPUserInfoEntity> ValidateUser(string user, string password)
        {
            LDAPResponseEntity<LDAPUserInfoEntity> response = new LDAPResponseEntity<LDAPUserInfoEntity>();

            try
            {
                Exception ex = new Exception("User or password is invalid");
                using (var pc = new PrincipalContext(ContextType.Domain, this.Domain, null, user, password))
                {

                    using (var foundUser = UserPrincipal.FindByIdentity(pc, IdentityType.SamAccountName, user))
                    {
                        if (foundUser != null)
                        {
                            PrincipalSearchResult<Principal> groups = foundUser.GetGroups();
                            response.Data = new LDAPUserInfoEntity();
                            response.Data.Group = ValidateGroups(groups);
                            response.Data.Name = foundUser.DisplayName;
                            response.Code = (int)LDAPEnum.OK;
                        }
                        else
                        {
                            throw ex;
                        }
                    }
                }
            }
            catch (LdapException ex)
            {
                switch (ex.ErrorCode)
                {
                    case 49:
                        response.Code = (int)LDAPEnum.VALIDATIONERROR;
                        response.Message = Localizator.GetStringResourceWithFormat("User", "UserOrPassInvalid");
                        break;
                    case 81:
                        response.Code = (int)LDAPEnum.ERROR;
                        response.Message = Localizator.GetStringResourceWithFormat("User", "LDAPServerUnavailable");
                        break;
                    default:
                        response.Code = (int)LDAPEnum.ERROR;
                        response.Message = ex.Message;
                        break;
                }
            }
            catch (Exception ex)
            {
                response.Code = (int)LDAPEnum.ERROR;
                response.Message = ex.Message;
            }

            return response;
        }

        private string ValidateGroups(PrincipalSearchResult<Principal> groups)
        {
            string group = "";
            Exception ex = new Exception("Usted no tiene permisos para acceder a la aplicación");
            if (groups.Any())
            {
                if (groups.Any(m => m.Name == AdminGroup))
                {
                    group = "Administrador";
                }
                else if (groups.Any(m => m.Name == AseGroup))
                {
                    group = "Asesor";
                }
                else if (groups.Any(m => m.Name == TecGroup))
                {
                    group = "Tecnico";
                }else
                {
                    throw ex;
                }
                return group;
            }
            throw ex;
        }
    }
}