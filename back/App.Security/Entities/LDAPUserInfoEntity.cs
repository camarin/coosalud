﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.Security.Entities
{
    public class LDAPUserInfoEntity
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string UserIdentification { get; set; }
        public string Phone { get; set; }
        public string CellPhone { get; set; }
        public long LastPasswordChange { get; set; }
        public string Address { get; set; }
        public string Group { get; set; }

    }
}