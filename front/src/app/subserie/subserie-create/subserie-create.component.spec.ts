import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubserieCreateComponent } from './subserie-create.component';

describe('SubserieCreateComponent', () => {
  let component: SubserieCreateComponent;
  let fixture: ComponentFixture<SubserieCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubserieCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubserieCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
