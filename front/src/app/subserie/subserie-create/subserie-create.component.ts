import { Component, OnInit, Inject } from '@angular/core';
import { BaseService } from 'src/app/base.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SerieContract } from 'src/app/contracts/serie.contract';
import { SubserieContract } from 'src/app/contracts/subserie.contract';

@Component({
  selector: 'app-subserie-create',
  templateUrl: './subserie-create.component.html',
  styleUrls: ['./subserie-create.component.css']
})
export class SubserieCreateComponent implements OnInit {
  serie: SerieContract;
  model: SubserieContract;
  constructor(
    private service: BaseService,
    public dialogRef: MatDialogRef<SubserieCreateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: SerieContract
  ) {
    this.serie = data;
    this.model = new SubserieContract();
    console.log(data);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {}

  save() {
    this.model.SerieId = this.serie.SerieId;
    this.service.Post<boolean>('/Subserie/Create', this.model).then(res => {
      this.dialogRef.close();
    });
  }
}
