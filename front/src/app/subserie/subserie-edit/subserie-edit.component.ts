import { Component, OnInit, Inject } from '@angular/core';
import { SerieContract } from 'src/app/contracts/serie.contract';
import { SubserieContract } from 'src/app/contracts/subserie.contract';
import { BaseService } from 'src/app/base.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-subserie-edit',
  templateUrl: './subserie-edit.component.html',
  styleUrls: ['./subserie-edit.component.css']
})
export class SubserieEditComponent implements OnInit {
  serie: SerieContract;
  model: SubserieContract;
  constructor(
    private service: BaseService,
    public dialogRef: MatDialogRef<SubserieEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: SubserieContract
  ) {
    this.model = { ...data };
  }

  onNoClick(): void {
    this.dialogRef.close(false);
  }

  ngOnInit() {}

  save() {
    this.service.Post<boolean>('/Subserie/Edit', { Id: this.model.SubserieId, Data: this.model }).then(res => {
      this.dialogRef.close(true);
    });
  }
}
