import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubserieEditComponent } from './subserie-edit.component';

describe('SubserieEditComponent', () => {
  let component: SubserieEditComponent;
  let fixture: ComponentFixture<SubserieEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubserieEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubserieEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
