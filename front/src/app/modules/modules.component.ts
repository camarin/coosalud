import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import { BaseService } from '../base.service';
import { DeleteConfirmationComponent } from '../delete-confirmation/delete-confirmation.component';
import { ModuleContract } from '../contracts/module.contract';
import { ModulesCreateComponent } from './modules-create/modules-create.component';
import { ModulesEditComponent } from './modules-edit/modules-edit.component';

@Component({
  selector: 'app-modules',
  templateUrl: './modules.component.html',
  styleUrls: ['./modules.component.css']
})
export class ModulesComponent implements OnInit {
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;

  displayedColumns: string[] = ['ModuleId', 'ModuleName', 'Acciones'];

  dataSource: MatTableDataSource<any>;
  constructor(private service: BaseService, private dialog: MatDialog) {}

  ngOnInit() {
    this.getdata();
  }

  getdata() {
    this.service.Get<Array<ModuleContract>>('/Module/GetAll', {}).then(res => {
      this.dataSource = new MatTableDataSource(res.Data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  create() {
    this.openDialog();
  }

  edit(elem: ModuleContract) {
    this.openEditDialog(elem);
  }

  delete(elem: ModuleContract) {
    const dialogRef = this.dialog.open(DeleteConfirmationComponent, {
      width: '500px',
      data: {
        url: '/Module/Delete',
        id: elem.ModuleId,
        title: 'Módulo'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getdata();
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(ModulesCreateComponent, {
      width: '500px'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getdata();
    });
  }

  openEditDialog(model: ModuleContract): void {
    const dialogRef = this.dialog.open(ModulesEditComponent, {
      width: '500px',
      data: model
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getdata();
    });
  }
}
