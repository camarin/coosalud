import { Component, OnInit, Inject } from '@angular/core';
import { ModuleContract } from 'src/app/contracts/module.contract';
import { BaseService } from 'src/app/base.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-modules-create',
  templateUrl: './modules-create.component.html',
  styleUrls: ['./modules-create.component.css']
})
export class ModulesCreateComponent implements OnInit {
  model: ModuleContract;
  constructor(
    private service: BaseService,
    public dialogRef: MatDialogRef<ModulesCreateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.model = new ModuleContract();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {}

  save() {
    this.service.Post<boolean>('/Module/Create', this.model).then(res => {
      this.dialogRef.close();
    });
  }
}
