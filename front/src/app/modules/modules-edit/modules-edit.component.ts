import { Component, OnInit, Inject } from '@angular/core';
import { ModuleContract } from 'src/app/contracts/module.contract';
import { BaseService } from 'src/app/base.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-modules-edit',
  templateUrl: './modules-edit.component.html',
  styleUrls: ['./modules-edit.component.css']
})
export class ModulesEditComponent implements OnInit {
  model: ModuleContract;
  constructor(
    private service: BaseService,
    public dialogRef: MatDialogRef<ModulesEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ModuleContract
  ) {
    this.model = data;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {}

  save() {
    this.service.Post<boolean>('/Module/Edit', { Id: this.model.ModuleId, Data: this.model }).then(res => {
      this.dialogRef.close();
    });
  }
}
