import { AfterViewInit, Component, OnDestroy } from '@angular/core';
import { MediaChange, ObservableMedia } from '@angular/flex-layout';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

const menu = [
  {
    title: 'Admin. Usuario',
    items: [
      {
        title: 'Usuario',
        url: 'users',
        icon: 'fa-user'
      },
      {
        title: 'Roles',
        url: 'roles',
        icon: 'fa-user-tag'
      },
      {
        title: 'Módulos',
        url: 'modules',
        icon: 'fa-puzzle-piece'
      }
    ]
  },
  {
    title: 'Maestras',
    items: [
      {
        title: 'Sucursales',
        url: 'entities',
        icon: 'fa-building'
      },
      {
        title: 'Areas',
        url: 'dependences',
        icon: 'fa-layer-group'
      },
      {
        title: 'Series y Subseries',
        url: 'series',
        icon: 'fa-folder-open'
      }
    ]
  },
  {
    title: 'TRD',
    items: [
      {
        title: 'Consulta',
        url: 'doctype',
        icon: 'fa-search'
      },
      {
        title: 'Carga Masiva',
        url: 'home',
        icon: 'fa-file-upload'
      }
    ]
  },
  {
    title: 'Inventario',
    items: [
      {
        title: 'Registro',
        url: 'inventory',
        icon: 'fa-keyboard'
      },
      {
        title: 'Transferencia',
        url: 'home',
        icon: 'fa-exchange-alt'
      }
    ]
  },
  {
    title: 'Reportes',
    items: [
      {
        title: 'Rotulo de Caja',
        url: 'home',
        icon: 'fa-archive'
      },
      {
        title: 'R. Transferencia',
        url: 'home',
        icon: 'fa-exchange-alt'
      },
      {
        title: 'Destrucción',
        url: 'home',
        icon: 'fa-trash'
      }
    ]
  }
];

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy, AfterViewInit {
  opened = true;
  mode: string;
  fixedInViewport: boolean;
  toggleNav: boolean;
  shouldStick = true;
  show = false;
  menu = menu;

  constructor(private router: Router, flex_media: ObservableMedia) {
    flex_media.subscribe((change: MediaChange) => {
      if (change.mqAlias === 'sm' || change.mqAlias === 'xs') {
        this.opened = false;
        this.mode = 'over';
        this.fixedInViewport = false;
        this.toggleNav = true;
      } else {
        this.opened = true;
        this.mode = 'side';
        this.fixedInViewport = true;
        this.toggleNav = false;
      }
    });
  }

  isActiveRoute(route: string) {
    return (
      this.router.serializeUrl(this.router.createUrlTree([this.router.url])) ===
      this.router.serializeUrl(this.router.createUrlTree([route]))
    );
  }

  goTo(route: string) {
    this.opened = false;
    this.router.navigate([route]);
  }

  triggerGoto(item: any) {
    item.opened = !item.opened;
    if (!item.items || item.items.length === 0) {
      this.goTo(item.url);
    }
  }

  closeSession() {
    localStorage.removeItem(environment.SESSION_KEY);
    this.router.navigate(['Login']);
  }

  ngOnDestroy(): void {}

  ngAfterViewInit() {}
}
