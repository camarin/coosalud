import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardGuard } from './auth-guard.guard';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { UsersComponent } from './users/users.component';
import { RoleComponent } from './role/role.component';
import { ModulesComponent } from './modules/modules.component';
import { EntityComponent } from './entity/entity.component';
import { DependenceComponent } from './dependence/dependence.component';
import { SerieComponent } from './serie/serie.component';
import { DoctypeComponent } from './doctype/doctype.component';
import { InventoryComponent } from './inventory/inventory.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuardGuard] },
  {
    path: 'users',
    component: UsersComponent,
    canActivate: [AuthGuardGuard]
  },
  {
    path: 'roles',
    component: RoleComponent,
    canActivate: [AuthGuardGuard]
  },
  {
    path: 'modules',
    component: ModulesComponent,
    canActivate: [AuthGuardGuard]
  },
  {
    path: 'entities',
    component: EntityComponent,
    canActivate: [AuthGuardGuard]
  },
  {
    path: 'dependences',
    component: DependenceComponent,
    canActivate: [AuthGuardGuard]
  },
  {
    path: 'series',
    component: SerieComponent,
    canActivate: [AuthGuardGuard]
  },
  {
    path: 'doctype',
    component: DoctypeComponent,
    canActivate: [AuthGuardGuard]
  },
  {
    path: 'inventory',
    component: InventoryComponent,
    canActivate: [AuthGuardGuard]
  },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
