import { HeaderContract } from './response-header.contract';

export class ResponseContract<T> {
  Header: HeaderContract;
  public Data: T;
  constructor() {
    this.Header = new HeaderContract();
  }
}
