import { InventoryContract } from './inventory.contract';

export class VwInventoryContract extends InventoryContract {
  InventoryStateName: string;
  SerieName: string;
  DependenceName: string;
  EntityName: string;
  SubserieName: string;
}
