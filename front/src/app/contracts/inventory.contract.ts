export class InventoryContract {
  constructor(){
    this.StartDate = new Date();
    this.EndDate = new Date();
    this.CreationDate = new Date();
    this.InventoryStateId = 1;
  }
  InventoryId: number;
  InventoryStateId: number;
  UserId: number;
  CreationDate: Date;
  EntityId: number;
  DependenceId: number;
  SerieId: number;
  SubserieId: number;
  StorageUnit: string;
  Tome: string;
  StartDate: Date;
  EndDate: Date;
  BoxCode: string;
  TransferDate: Date;
}
