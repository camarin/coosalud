export class DoctypeContract {
  DocumentalTypeId: number;
  SubserieId: number;
  Code: string;
  DocumentalTypeName: string;
  GeneralWithholding: string;
  CentralWithholding: string;
  PaperSupport: string;
  ElectronicSupport: string;
  DisposalElimination: string;
  DisposalSelection: string;
  DisposalTotalConservation: string;
  DisposalDigitalization: string;
}
