export class SubserieContract {
  SubserieId: number;
  SerieId: number;
  Code: string;
  SubserieName: string;
  Process: string;
}
