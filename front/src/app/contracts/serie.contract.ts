export class SerieContract {
  SerieId: number;
  DependenceId: number;
  Code: string;
  SerieName: string;
}
