export class DependenceContract {
  DependenceId: number;
  EntityId: number;
  Code: string;
  DependenceName: string;
}
