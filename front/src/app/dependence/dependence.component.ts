import { Component, OnInit, ViewChild } from '@angular/core';
import { DependenceContract } from '../contracts/dependence.contract';
import { MatTableDataSource, MatDialog, MatPaginator, MatSort } from '@angular/material';
import { BaseService } from '../base.service';
import { DeleteConfirmationComponent } from '../delete-confirmation/delete-confirmation.component';
import { DependenceCreateComponent } from './dependence-create/dependence-create.component';
import { DependenceEditComponent } from './dependence-edit/dependence-edit.component';
import { VwDependenceContract } from '../contracts/vw.dependence.contract';

@Component({
  selector: 'app-dependence',
  templateUrl: './dependence.component.html',
  styleUrls: ['./dependence.component.css']
})
export class DependenceComponent implements OnInit {
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;

  displayedColumns: string[] = ['DependenceId', 'DependenceName', 'EntityName', 'Code', 'Acciones'];

  dataSource: MatTableDataSource<any>;
  constructor(private service: BaseService, private dialog: MatDialog) {}

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.service.Get<Array<VwDependenceContract>>('/Dependence/GetVwAll', {}).then(res => {
      this.dataSource = new MatTableDataSource(res.Data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  create() {
    this.openCreateDialog();
  }

  edit(elem) {
    this.openEditDialog(elem);
  }

  delete(elem: DependenceContract) {
    const dialogRef = this.dialog.open(DeleteConfirmationComponent, {
      width: '700px',
      data: {
        url: '/Dependence/Delete',
        id: elem.DependenceId,
        title: 'Dependencia'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getData();
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openEditDialog(model: DependenceContract): void {
    const dialogRef = this.dialog.open(DependenceEditComponent, {
      width: '500px',
      data: model
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getData();
    });
  }

  openCreateDialog(): void {
    const dialogRef = this.dialog.open(DependenceCreateComponent, {
      width: '700px'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getData();
    });
  }
}
