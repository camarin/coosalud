import { Component, OnInit, Inject } from '@angular/core';
import { DependenceContract } from '../../contracts/dependence.contract';
import { BaseService } from 'src/app/base.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DependenceCreateComponent } from '../dependence-create/dependence-create.component';
import { ModuleContract } from 'src/app/contracts/module.contract';
import { FormControl } from '@angular/forms';
import { EntityContract } from 'src/app/contracts/entity.contract';

@Component({
  selector: 'app-dependence-edit',
  templateUrl: './dependence-edit.component.html',
  styleUrls: ['./dependence-edit.component.css']
})
export class DependenceEditComponent implements OnInit {
  model: DependenceContract;
  entities: Array<EntityContract>;

  constructor(
    private service: BaseService,
    public dialogRef: MatDialogRef<DependenceCreateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DependenceContract
  ) {
    this.model = data;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
    this.service.Get<Array<EntityContract>>('/Entity/GetAll', {}).then(res => {
      this.entities = res.Data;
    });
  }

  save() {
    this.service.Post<boolean>('/Dependence/Edit', { Id: this.model.DependenceId, Data: this.model }).then(res => {
      this.dialogRef.close();
    });
  }
}
