import { Component, OnInit, Inject } from '@angular/core';
import { DependenceContract } from '../../contracts/dependence.contract';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { BaseService } from 'src/app/base.service';
import { EntityContract } from 'src/app/contracts/entity.contract';

@Component({
  selector: 'app-dependence-create',
  templateUrl: './dependence-create.component.html',
  styleUrls: ['./dependence-create.component.css']
})
export class DependenceCreateComponent implements OnInit {
  model: DependenceContract;
  entities: Array<EntityContract>;

  constructor(
    private service: BaseService,
    public dialogRef: MatDialogRef<DependenceCreateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.model = new DependenceContract();
    this.entities = [];
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
    this.service.Get<Array<EntityContract>>('/Entity/GetAll', {}).then(res => {
      this.entities = res.Data;
    });
  }

  save() {
    this.service.Post<boolean>('/Dependence/Create', this.model).then(res => {
      this.dialogRef.close();
    });
  }
}
