import { Component, OnInit, Inject } from '@angular/core';
import { DoctypeContract } from '../contracts/doctype.contract';
import { SubserieContract } from '../contracts/subserie.contract';
import { BaseService } from '../base.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DoctypeCreateComponent } from '../doctype-create/doctype-create.component';

@Component({
  selector: 'app-doctype-edit',
  templateUrl: './doctype-edit.component.html',
  styleUrls: ['./doctype-edit.component.css']
})
export class DoctypeEditComponent implements OnInit {
  model: DoctypeContract;
  subseries: Array<SubserieContract>;

  constructor(
    private service: BaseService,
    public dialogRef: MatDialogRef<DoctypeCreateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DoctypeContract
  ) {
    this.model = data;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
    this.service.Get<Array<SubserieContract>>('/Subserie/GetAll', {}).then(res => {
      this.subseries = res.Data;
    });
  }

  save() {
    this.service.Post<boolean>('/DocumentalType/Edit', { Id: this.model.DocumentalTypeId, Data: this.model }).then(res => {
      this.dialogRef.close();
    });
  }
}
