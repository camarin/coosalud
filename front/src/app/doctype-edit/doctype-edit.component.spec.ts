import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctypeEditComponent } from './doctype-edit.component';

describe('DoctypeEditComponent', () => {
  let component: DoctypeEditComponent;
  let fixture: ComponentFixture<DoctypeEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoctypeEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctypeEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
