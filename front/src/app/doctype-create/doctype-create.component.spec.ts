import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctypeCreateComponent } from './doctype-create.component';

describe('DoctypeCreateComponent', () => {
  let component: DoctypeCreateComponent;
  let fixture: ComponentFixture<DoctypeCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoctypeCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctypeCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
