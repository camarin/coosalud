import { Component, OnInit, Inject } from '@angular/core';
import { DoctypeContract } from '../contracts/doctype.contract';
import { BaseService } from '../base.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SubserieContract } from '../contracts/subserie.contract';

@Component({
  selector: 'app-doctype-create',
  templateUrl: './doctype-create.component.html',
  styleUrls: ['./doctype-create.component.css']
})
export class DoctypeCreateComponent implements OnInit {
  model: DoctypeContract;
  subseries: Array<SubserieContract>;

  constructor(
    private service: BaseService,
    public dialogRef: MatDialogRef<DoctypeCreateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.model = new DoctypeContract();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
    this.service.Get<Array<SubserieContract>>('/Subserie/GetAll', {}).then(res => {
      this.subseries = res.Data;
    });
  }

  save() {
    this.service.Post<boolean>('/DocumentalType/Create', this.model).then(res => {
      this.dialogRef.close();
    });
  }
}
