import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { BaseService } from '../base.service';

@Component({
  selector: 'app-delete-confirmation',
  templateUrl: './delete-confirmation.component.html',
  styleUrls: ['./delete-confirmation.component.scss']
})
export class DeleteConfirmationComponent implements OnInit {
  deleting = false;
  constructor(
    private service: BaseService,
    public dialogRef: MatDialogRef<DeleteConfirmationComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      url: string;
      id: any;
      title: string;
    }
  ) {}

  ngOnInit() {}

  onNoClick(): void {
    this.dialogRef.close(false);
  }

  delete() {
    this.deleting = true;
    this.service
      .Post(this.data.url, { Id: this.data.id })
      .then(res => {
        this.dialogRef.close(true);
      })
      .catch(error => (this.deleting = false));
  }
}
