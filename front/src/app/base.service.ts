import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { environment } from '../environments/environment';
import { ResponseContract } from './contracts/response.contract';
import { MatSnackBar } from '@angular/material';
declare var $: any;

@Injectable()
export class BaseService {
  private storage = window.sessionStorage;
  private Headers: Headers;
  private Options: RequestOptions;

  constructor(private http: Http, private snackBar: MatSnackBar) {
    this.Headers = this._getHeaders();
    this.Options = new RequestOptions({
      headers: this.Headers
    });
  }

  getItem(key: string) {
    return this.storage.getItem(key);
  }

  getSerializeItem<T>(key: string) {
    return <T>JSON.parse(this.storage.getItem(key));
  }

  setItem(key: string, data: any) {
    this.storage.setItem(key, JSON.stringify(data));
  }

  removeItem(key: string) {
    this.storage.removeItem(key);
  }

  Post<T>(
    url: string,
    data: object,
    authorization: boolean = false
  ): Promise<ResponseContract<T>> {
    // if (authorization && !this.Options.headers.has('Authorization')) {
    //   this.Options.headers.append(
    //     'Authorization',
    //     'Bearer ' + this.getUserToken()
    //   );
    // }
    // return this.http
    //   .post(`${environment.API}${url}`, JSON.stringify(data), this.Options)
    //   .subscribe(data => {})
    //   .toPromise()
    //   .then(res => {
    //     return this.extractData(res);
    //   })
    //   .catch(error => {
    //     return this.handleError(error);
    //   });

    return new Promise((resolver, reject) => {
      this.http
        .post(`${environment.API}${url}`, JSON.stringify(data), this.Options)
        .subscribe(
          res => {
            resolver(this.extractData(res));
          },
          error => {
            reject(this.handleError(error));
          }
        );
    });
  }

  private _getHeaders(): Headers {
    const header = new Headers({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    });

    return header;
  }

  Get<T>(
    url: string,
    data: object,
    authorization: boolean = true,
    loadingContent: any = null
  ): Promise<ResponseContract<T>> {
    // if (authorization && !this.Options.headers.has('Authorization')) {
    //   this.Options.headers.append(
    //     'Authorization',
    //     'Bearer ' + this.getUserToken()
    //   );
    // }
    // return this.http
    //   .get(`${environment.API}/${url}`)
    //   .toPromise()
    //   .then(res => {
    //     return this.extractData(res);
    //   })
    //   .catch(error => {
    //     return this.handleError(error);
    //   });

    return new Promise((resolver, reject) => {
      this.http.get(`${environment.API}${url}`, this.Options).subscribe(
        res => {
          resolver(this.extractData(res));
        },
        error => {
          reject(this.handleError(error));
        }
      );
    });
  }

  private extractData(value: any) {
    const response = value.json();
    if (
      response &&
      response.Header.Status !== 200 &&
      response.Header.Status !== 202
    ) {
      this.snackBar.open(response.Header.Message, '', {
        duration: 3000
      });
      return Promise.reject(response);
    }
    if (response && response.Header.Status === 202) {
    }

    return response;
  }

  private handleError(error: any): Promise<any> {
    this.snackBar.open('Ha ocurrido un error inesperado', '', {
      duration: 10000
    });
    return Promise.reject(error);
  }
}
