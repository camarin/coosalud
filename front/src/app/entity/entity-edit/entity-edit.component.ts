import { Component, OnInit, Inject } from '@angular/core';
import { EntityContract } from '../../contracts/entity.contract';
import { BaseService } from 'src/app/base.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { EntityCreateComponent } from '../entity-create/entity-create.component';
import { ModuleContract } from 'src/app/contracts/module.contract';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-entity-edit',
  templateUrl: './entity-edit.component.html',
  styleUrls: ['./entity-edit.component.css']
})
export class EntityEditComponent implements OnInit {
  model: EntityContract;
  modules: Array<ModuleContract>;
  toppings = new FormControl();
  entityModules: Array<number>;

  constructor(
    private service: BaseService,
    public dialogRef: MatDialogRef<EntityCreateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: EntityContract
  ) {
    this.model = data;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {}

  save() {
    this.service.Post<boolean>('/Entity/Edit', { Id: this.model.EntityId, Data: this.model }).then(res => {
      this.dialogRef.close();
    });
  }
}
