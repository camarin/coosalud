import { Component, OnInit, Inject } from '@angular/core';
import { EntityContract } from '../../contracts/entity.contract';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { BaseService } from 'src/app/base.service';

@Component({
  selector: 'app-entity-create',
  templateUrl: './entity-create.component.html',
  styleUrls: ['./entity-create.component.css']
})
export class EntityCreateComponent implements OnInit {
  model: EntityContract;

  constructor(
    private service: BaseService,
    public dialogRef: MatDialogRef<EntityCreateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.model = new EntityContract();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {

  }

  save() {
    this.service.Post<boolean>('/Entity/Create', this.model).then(res => {
      this.dialogRef.close();
    });
  }
}
