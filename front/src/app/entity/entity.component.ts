import { Component, OnInit, ViewChild } from '@angular/core';
import { EntityContract } from '../contracts/entity.contract';
import { MatTableDataSource, MatDialog, MatPaginator, MatSort } from '@angular/material';
import { BaseService } from '../base.service';
import { DeleteConfirmationComponent } from '../delete-confirmation/delete-confirmation.component';
import { EntityCreateComponent } from './entity-create/entity-create.component';
import { EntityEditComponent } from './entity-edit/entity-edit.component';

@Component({
  selector: 'app-entity',
  templateUrl: './entity.component.html',
  styleUrls: ['./entity.component.css']
})
export class EntityComponent implements OnInit {
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;

  displayedColumns: string[] = ['EntityId', 'EntityName', 'Acciones'];

  dataSource: MatTableDataSource<any>;
  constructor(private service: BaseService, private dialog: MatDialog) {}

  ngOnInit() {
    this.getData();
  }
  getData() {
    this.service.Get<Array<EntityContract>>('/Entity/GetAll', {}).then(res => {
      this.dataSource = new MatTableDataSource(res.Data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  create() {
    this.openCreateDialog();
  }

  edit(elem) {
    this.openEditDialog(elem);
  }

  delete(elem: EntityContract) {
    const dialogRef = this.dialog.open(DeleteConfirmationComponent, {
      width: '700px',
      data: {
        url: '/Entity/Delete',
        id: elem.EntityId,
        title: 'Entidad'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getData();
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openEditDialog(model: EntityContract): void {
    const dialogRef = this.dialog.open(EntityEditComponent, {
      width: '500px',
      data: model
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getData();
    });
  }

  openCreateDialog(): void {
    const dialogRef = this.dialog.open(EntityCreateComponent, {
      width: '700px'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getData();
    });
  }
}
