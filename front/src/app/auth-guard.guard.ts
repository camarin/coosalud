import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import { Observable } from 'rxjs';
import { BaseService } from './base.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardGuard implements CanActivate {
  constructor(private router: Router, private service: BaseService) { }
  async canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return true;
    // const module_key = state.url.replace('/', '');
    // const userdata = localStorage.getItem(environment.SESSION_KEY);
    // if (userdata) {
    //   if (module_key !== 'Home') {
    //     try {
    //       const model = { ...JSON.parse(userdata), ModuleKey: module_key };
    //       const data = await this.service.Post<Boolean>(
    //         '/Users/RequestPermissions',
    //         model
    //       );
    //       if (data && data.Data) {
    //         return true;
    //       } else {
    //         this.router.navigate(['Login']);
    //         return false;
    //       }
    //     } catch (ex) {
    //       this.router.navigate(['Home']);
    //       return false;
    //     }
    //   }
    //   return true;
    // } else {
    //   this.router.navigate(['Login']);
    //   return false;
    // }
  }
}
