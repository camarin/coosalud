import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import { BaseService } from '../base.service';
import { DoctypeContract } from '../contracts/doctype.contract';
import { DeleteConfirmationComponent } from '../delete-confirmation/delete-confirmation.component';
import { DoctypeEditComponent } from '../doctype-edit/doctype-edit.component';
import { DoctypeCreateComponent } from '../doctype-create/doctype-create.component';

@Component({
  selector: 'app-doctype',
  templateUrl: './doctype.component.html',
  styleUrls: ['./doctype.component.css']
})
export class DoctypeComponent implements OnInit {
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;

  displayedColumns: string[] = ['DocumentalTypeId', 'DocumentalTypeName', 'Acciones'];

  dataSource: MatTableDataSource<any>;
  constructor(private service: BaseService, private dialog: MatDialog) {}

  ngOnInit() {
    this.getData();
  }
  getData() {
    this.service.Get<Array<DoctypeContract>>('/DocumentalType/GetAll', {}).then(res => {
      this.dataSource = new MatTableDataSource(res.Data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  create() {
    this.openCreateDialog();
  }

  edit(elem) {
    this.openEditDialog(elem);
  }

  delete(elem: DoctypeContract) {
    const dialogRef = this.dialog.open(DeleteConfirmationComponent, {
      width: '700px',
      data: {
        url: '/DocumentalType/Delete',
        id: elem.DocumentalTypeId,
        title: 'Tipo documental'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getData();
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openEditDialog(model: DoctypeContract): void {
    const dialogRef = this.dialog.open(DoctypeEditComponent, {
      width: '700px',
      data: model
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getData();
    });
  }

  openCreateDialog(): void {
    const dialogRef = this.dialog.open(DoctypeCreateComponent, {
      width: '700px'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getData();
    });
  }
}
