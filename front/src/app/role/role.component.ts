import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { BaseService } from '../base.service';
import { RoleCreateComponent } from './role-create/role-create.component';
import { RoleEditComponent } from './role-edit/role-edit.component';
import { RoleContract } from '../contracts/role.contract';
import { DeleteConfirmationComponent } from '../delete-confirmation/delete-confirmation.component';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css']
})
export class RoleComponent implements OnInit {
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;

  displayedColumns: string[] = ['RoleId', 'RoleName', 'Acciones'];

  dataSource: MatTableDataSource<any>;
  constructor(private service: BaseService, private dialog: MatDialog) {}

  ngOnInit() {
    this.getData();
  }
  getData() {
    this.service.Get<Array<RoleContract>>('/Role/GetAll', {}).then(res => {
      this.dataSource = new MatTableDataSource(res.Data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  create() {
    this.openCreateDialog();
  }

  edit(elem) {
    this.openEditDialog(elem);
  }

  delete(elem: RoleContract) {
    const dialogRef = this.dialog.open(DeleteConfirmationComponent, {
      width: '700px',
      data: {
        url: '/Role/Delete',
        id: elem.RoleId,
        title: 'Rol'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getData();
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openEditDialog(model: RoleContract): void {
    const dialogRef = this.dialog.open(RoleEditComponent, {
      width: '500px',
      data: model
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getData();
    });
  }

  openCreateDialog(): void {
    const dialogRef = this.dialog.open(RoleCreateComponent, {
      width: '700px'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getData();
    });
  }
}
