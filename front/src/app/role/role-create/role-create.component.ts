import { Component, OnInit, Inject } from '@angular/core';
import { RoleContract } from '../../contracts/role.contract';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { BaseService } from 'src/app/base.service';
import { FormControl } from '@angular/forms';
import { ModuleContract } from 'src/app/contracts/module.contract';

@Component({
  selector: 'app-role-create',
  templateUrl: './role-create.component.html',
  styleUrls: ['./role-create.component.css']
})
export class RoleCreateComponent implements OnInit {
  model: RoleContract;

  constructor(
    private service: BaseService,
    public dialogRef: MatDialogRef<RoleCreateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.model = new RoleContract();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {

  }

  save() {
    this.service.Post<boolean>('/Role/Create', this.model).then(res => {
      this.dialogRef.close();
    });
  }
}
