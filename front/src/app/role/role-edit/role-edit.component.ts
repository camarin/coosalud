import { Component, OnInit, Inject } from '@angular/core';
import { RoleContract } from '../../contracts/role.contract';
import { BaseService } from 'src/app/base.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { RoleCreateComponent } from '../role-create/role-create.component';
import { ModuleContract } from 'src/app/contracts/module.contract';
import { FormControl } from '@angular/forms';
import { RoleModuleContract } from 'src/app/contracts/role-module.contract';

@Component({
  selector: 'app-role-edit',
  templateUrl: './role-edit.component.html',
  styleUrls: ['./role-edit.component.css']
})
export class RoleEditComponent implements OnInit {
  model: RoleContract;
  modules: Array<ModuleContract>;
  toppings = new FormControl();
  roleModules: Array<number>;

  modulesInRole: Array<RoleModuleContract>;

  constructor(
    private service: BaseService,
    public dialogRef: MatDialogRef<RoleCreateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: RoleContract
  ) {
    this.model = data;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
    this.service.Get<Array<ModuleContract>>('/Module/GetAll', {}).then(res => {
      this.modules = res.Data;
    });

    this.service.Get<Array<RoleModuleContract>>('/RoleModule/GetAll', {}).then(res => {
      this.modulesInRole = res.Data;
      this.roleModules = this.modulesInRole.filter(m => m.RoleId === this.model.RoleId).map(m => m.ModuleId);
    });
  }

  save() {
    this.service.Post<boolean>('/Role/EditWithModules', { Role: this.model, Modules: this.roleModules }).then(res => {
      this.dialogRef.close();
    });
  }
}
