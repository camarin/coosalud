import { Component, OnInit, Inject } from '@angular/core';
import { SerieContract } from '../../contracts/serie.contract';
import { BaseService } from 'src/app/base.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SerieCreateComponent } from '../serie-create/serie-create.component';
import { ModuleContract } from 'src/app/contracts/module.contract';
import { FormControl } from '@angular/forms';
import { EntityContract } from 'src/app/contracts/entity.contract';
import { DependenceContract } from 'src/app/contracts/dependence.contract';

@Component({
  selector: 'app-serie-edit',
  templateUrl: './serie-edit.component.html',
  styleUrls: ['./serie-edit.component.css']
})
export class SerieEditComponent implements OnInit {
  model: SerieContract;
  entities: Array<EntityContract>;
  dependences: Array<DependenceContract>;
  disabledDependence = true;
  selectedDependence: DependenceContract = new DependenceContract();

  constructor(
    private service: BaseService,
    public dialogRef: MatDialogRef<SerieCreateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: SerieContract
  ) {
    this.model = data;
  }

  onEntitySelectChange(val) {
    this.disabledDependence = true;
    this.service.Get<Array<DependenceContract>>('/Dependence/GetAll', {}).then(res => {
      this.dependences = res.Data.filter(m => m.EntityId === val.value);
      if (this.dependences.length > 0) {
        this.disabledDependence = false;
      }
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
    this.service.Get<Array<EntityContract>>('/Entity/GetAll', {}).then(res => {
      this.entities = res.Data;
    });

    this.service.Get<Array<DependenceContract>>('/Dependence/GetAll', {}).then(res => {
      this.selectedDependence = res.Data.find(m => m.DependenceId === this.model.DependenceId);
      this.dependences = res.Data.filter(m => m.EntityId === this.selectedDependence.EntityId);
      this.disabledDependence = false;
    });
  }

  save() {
    this.service.Post<boolean>('/Serie/Edit', { Id: this.model.SerieId, Data: this.model }).then(res => {
      this.dialogRef.close();
    });
  }
}
