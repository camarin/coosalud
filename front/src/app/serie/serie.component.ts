import { Component, OnInit, ViewChild } from '@angular/core';
import { SerieContract } from '../contracts/serie.contract';
import { MatTableDataSource, MatDialog, MatPaginator, MatSort } from '@angular/material';
import { BaseService } from '../base.service';
import { DeleteConfirmationComponent } from '../delete-confirmation/delete-confirmation.component';
import { SerieCreateComponent } from './serie-create/serie-create.component';
import { SerieEditComponent } from './serie-edit/serie-edit.component';
import { SubserieCreateComponent } from '../subserie/subserie-create/subserie-create.component';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { SubserieContract } from '../contracts/subserie.contract';
import { SubserieEditComponent } from '../subserie/subserie-edit/subserie-edit.component';

@Component({
  selector: 'app-serie',
  templateUrl: './serie.component.html',
  styleUrls: ['./serie.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'))
    ])
  ]
})
export class SerieComponent implements OnInit {
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;
  displayedColumns: string[] = ['SerieId', 'SerieName', 'Acciones'];
  displayedSubColumns: string[] = ['SubserieId', 'SubserieName', 'Code', 'Acciones'];
  dataSource: MatTableDataSource<any>;
  subseries: MatTableDataSource<any>;
  expandedElement: SerieContract | null;

  constructor(private service: BaseService, private dialog: MatDialog) {}

  ngOnInit() {
    this.getData();
  }
  getData() {
    this.service.Get<Array<SerieContract>>('/Serie/GetAll', {}).then(res => {
      this.dataSource = new MatTableDataSource(res.Data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  create() {
    this.openCreateDialog();
  }

  getSubseries(elem: SerieContract) {
    this.subseries = new MatTableDataSource([]);
    this.service.Get<Array<SubserieContract>>('/Subserie/GetAll', {}).then(res => {
      this.subseries = new MatTableDataSource(res.Data.filter(m => m.SerieId === elem.SerieId));
    });
  }

  edit(elem) {
    this.openEditDialog(elem);
  }

  addSubserie(elem) {
    const dialogRef = this.dialog.open(SubserieCreateComponent, {
      width: '700px',
      data: elem
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getData();
    });
  }

  deleteSub(elem: SubserieContract) {
    const dialogRef = this.dialog.open(DeleteConfirmationComponent, {
      width: '700px',
      data: {
        url: '/Subserie/Delete',
        id: elem.SubserieId,
        title: 'Subserie'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getData();
    });
  }

  editSub(elem: SubserieContract) {
    const dialogRef = this.dialog.open(SubserieEditComponent, {
      width: '500px',
      data: elem
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getData();
      }
    });
  }

  delete(elem: SerieContract) {
    const dialogRef = this.dialog.open(DeleteConfirmationComponent, {
      width: '700px',
      data: {
        url: '/Serie/Delete',
        id: elem.SerieId,
        title: 'Serie'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getData();
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openEditDialog(model: SerieContract): void {
    const dialogRef = this.dialog.open(SerieEditComponent, {
      width: '500px',
      data: model
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getData();
    });
  }

  openCreateDialog(): void {
    const dialogRef = this.dialog.open(SerieCreateComponent, {
      width: '700px'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getData();
    });
  }
}
