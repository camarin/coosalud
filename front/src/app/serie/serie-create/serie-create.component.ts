import { Component, OnInit, Inject } from '@angular/core';
import { SerieContract } from '../../contracts/serie.contract';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { BaseService } from 'src/app/base.service';
import { EntityContract } from 'src/app/contracts/entity.contract';
import { DependenceContract } from 'src/app/contracts/dependence.contract';

@Component({
  selector: 'app-serie-create',
  templateUrl: './serie-create.component.html',
  styleUrls: ['./serie-create.component.css']
})
export class SerieCreateComponent implements OnInit {
  model: SerieContract;
  entities: Array<EntityContract>;
  dependences: Array<DependenceContract>;
  disabledDependence = true;


  constructor(
    private service: BaseService,
    public dialogRef: MatDialogRef<SerieCreateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.model = new SerieContract();
  }

  onEntitySelectChange(val) {
    this.disabledDependence = true;
    this.service.Get<Array<DependenceContract>>('/Dependence/GetAll', {}).then(res => {
      this.dependences = res.Data.filter(m => m.EntityId === val.value);
      if (this.dependences.length > 0) {
        this.disabledDependence = false;
      }
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.service.Get<Array<EntityContract>>('/Entity/GetAll', {}).then(res => {
      this.entities = res.Data;
    });
  }

  save() {
    this.service.Post<boolean>('/Serie/Create', this.model).then(res => {
      this.dialogRef.close();
    });
  }
}
