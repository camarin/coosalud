import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog, MatSnackBar } from '@angular/material';
import { BaseService } from '../base.service';
import { InventoryContract } from '../contracts/inventory.contract';
import { DeleteConfirmationComponent } from '../delete-confirmation/delete-confirmation.component';
import { EntityContract } from '../contracts/entity.contract';
import { DependenceContract } from '../contracts/dependence.contract';
import { SerieContract } from '../contracts/serie.contract';
import { SubserieContract } from '../contracts/subserie.contract';
import { SelectionModel } from '@angular/cdk/collections';
import { InventoryStateContract } from '../contracts/inventory-state.contract';
import { VwInventoryContract } from '../contracts/vw.inventory.contract';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.css']
})
export class InventoryComponent implements OnInit {
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;

  model: InventoryContract;
  editModel: InventoryContract;

  dependences: Array<DependenceContract>;
  AllSeries: Array<SerieContract>;
  series: Array<SerieContract>;

  inventoryState: Array<InventoryStateContract>;

  AllSubseries: Array<SubserieContract>;
  subseries: Array<SubserieContract>;

  creating = false;
  editing = false;

  displayedColumns: string[] = ['select', 'InventoryId', 'InventoryStateName',
  'DependenceName', 'SerieName', 'SubserieName', 'StorageUnit', 'Tome', 'Acciones'];

  dataSource: MatTableDataSource<any>;
  selection = new SelectionModel<InventoryContract>(true, []);
  constructor(private service: BaseService, private dialog: MatDialog, private snackBar: MatSnackBar) {
    this.model = new InventoryContract();
    this.AllSeries = [];
    this.series = [];
    this.dataSource = new MatTableDataSource([]);
    this.initEditModel();
  }

  private initEditModel() {
    this.editModel = new InventoryContract();
    this.editModel.CreationDate = null;
    this.editModel.StartDate = null;
    this.editModel.EndDate = null;
    this.editModel.InventoryStateId = null;
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ? this.selection.clear() : this.dataSource.data.forEach(row => this.selection.select(row));
  }

  selectRow() {
    console.log(this.selection.selected);
    this.creating = false;
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: InventoryContract): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.InventoryId + 1}`;
  }

  ngOnInit() {
    this.getData();
    this.service.Get<Array<DependenceContract>>('/Dependence/GetAll', {}).then(res => {
      this.dependences = res.Data;
    });

    this.service.Get<Array<SerieContract>>('/Serie/GetAll', {}).then(res => {
      this.AllSeries = res.Data;
    });

    this.service.Get<Array<SubserieContract>>('/Subserie/GetAll', {}).then(res => {
      this.AllSubseries = res.Data;
    });

    this.service.Get<Array<InventoryStateContract>>('/InventoryState/GetAll', {}).then(res => {
      this.inventoryState = res.Data;
    });
  }
  getData() {
    this.service.Get<Array<VwInventoryContract>>('/Inventory/GetVwAll', {}).then(res => {
      this.dataSource = new MatTableDataSource(res.Data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  create() {
    this.creating = true;
    this.model = new InventoryContract();
    this.series = [];
    this.subseries = [];
  }

  cancelCreate() {
    this.creating = false;
  }

  save() {
    this.model.InventoryStateId = 1;
    this.service.Post<boolean>('/Inventory/Create', this.model).then(res => {
      this.getData();
    });
  }

  delete(elem: InventoryContract) {
    const dialogRef = this.dialog.open(DeleteConfirmationComponent, {
      width: '700px',
      data: {
        url: '/Inventory/Delete',
        id: elem.InventoryId,
        title: 'Documento'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getData();
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  onDependenceSelectChange(ev) {
    this.series = this.AllSeries.filter(m => m.DependenceId === ev.value);
    this.subseries = [];
  }

  onSerieSelectChange(ev) {
    this.subseries = this.AllSubseries.filter(m => m.SerieId === ev.value);
  }

  isEditing() {
    return this.selection.selected.length > 0;
  }

  cancelEdit() {
    this.selection = new SelectionModel<InventoryContract>(true, []);
  }

  edit() {
    console.log(this.editModel);

    if (this.editModel.DependenceId && (this.editModel.SerieId === undefined || this.editModel.SubserieId === undefined)) {
      this.snackBar.open('La serie y la subserie son requeridas', '', {
        duration: 10000
      });
    } else {
      for (const prop in this.editModel) {
        if (this.editModel[prop]) {
          this.selection.selected.forEach(v => {
            v[prop] = this.editModel[prop];
          });
        }
      }
      const promises = [];
      this.editing = true;
      this.selection.selected.forEach(v => {
        promises.push(this.service.Post<boolean>('/Inventory/Edit', { Id: v.InventoryId, Data: v }));
      });
      Promise.all(promises)
        .then(res => {
          this.getData();
          this.editing = false;
          this.snackBar.open('Se han actualizado exitosamente ' + this.selection.selected.length + ' registros', '', {
            duration: 10000
          });
          this.cancelEdit();
          this.initEditModel();
        })
        .catch(err => {
          this.editing = false;
        });
    }
  }
}
