import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UserContract } from '../../contracts/user.contract';
import { BaseService } from 'src/app/base.service';
import { RoleContract } from 'src/app/contracts/role.contract';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent implements OnInit {
  model: UserContract;
  roleModel: Array<RoleContract>;
  constructor(
    private service: BaseService,
    public dialogRef: MatDialogRef<UserCreateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: UserContract
  ) {
    this.model = data;
    this.roleModel = [];
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
    this.service.Get<Array<RoleContract>>('/Role/GetAll', {}).then(res => {
      this.roleModel = res.Data;
    });
  }

  save() {
    this.service.Post<boolean>('/Users/Edit', { Id: this.model.UserId, Data: this.model }).then(res => {
      this.dialogRef.close();
    });
  }
}
