import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import { BaseService } from '../base.service';
import { UserContract } from '../contracts/user.contract';
import { UserCreateComponent } from './user-create/user-create.component';
import { VwUserContract } from '../contracts/vw.user.contract';
import { DeleteConfirmationComponent } from '../delete-confirmation/delete-confirmation.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;

  displayedColumns: string[] = ['UserId', 'UserName', 'State', 'RoleName', 'Acciones'];

  dataSource: MatTableDataSource<any>;
  constructor(private service: BaseService, private dialog: MatDialog) {}

  ngOnInit() {
    this.getdata();
  }

  getdata() {
    this.service.Get<Array<VwUserContract>>('/Users/GetVwAll', {}).then(res => {
      this.dataSource = new MatTableDataSource(res.Data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  create() {}

  edit(elem) {
    this.openDialog(elem);
  }

  delete(elem: UserContract) {
    const dialogRef = this.dialog.open(DeleteConfirmationComponent, {
      width: '500px',
      data: {
        url: '/Users/Delete',
        id: elem.UserId,
        title: 'Usuario'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getdata();
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openDialog(model: UserContract): void {
    const dialogRef = this.dialog.open(UserCreateComponent, {
      width: '500px',
      data: model
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getdata();
    });
  }
}
