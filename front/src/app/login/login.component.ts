import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { LoginRequestContract } from '../contracts/login-request.contract';
import { BaseService } from '../base.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  model: LoginRequestContract;
  loading = false;
  constructor(private router: Router, private service: BaseService) {
    this.model = new LoginRequestContract();
  }

  async login() {
    this.loading = true;
    this.router.navigate(['home']);
    // try {
    //   const data = await this.service.Post<any>('/Users/Login', this.model);
    //   localStorage.setItem(environment.SESSION_KEY, JSON.stringify(data.Data));
    // } catch (ex) {
    // }
    this.loading = false;
  }
}
