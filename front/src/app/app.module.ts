import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MatOptionModule, MatPaginatorModule, MatSelectModule, MatSortModule } from '@angular/material';
import { MatButtonModule } from '@angular/material/button';
import { MatNativeDateModule } from '@angular/material';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule, MatIconRegistry } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTreeModule } from '@angular/material/tree';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { ServiceWorkerModule } from '@angular/service-worker';
import { DynamicFormsCoreModule } from '@ng-dynamic-forms/core';
import { DynamicFormsMaterialUIModule } from '@ng-dynamic-forms/ui-material';
import { NgxMaskModule } from 'ngx-mask';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BaseService } from './base.service';
import { DeleteConfirmationComponent } from './delete-confirmation/delete-confirmation.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RoleCreateComponent } from './role/role-create/role-create.component';
import { RoleEditComponent } from './role/role-edit/role-edit.component';
import { RoleComponent } from './role/role.component';
import { UserCreateComponent } from './users/user-create/user-create.component';
import { UsersComponent } from './users/users.component';
import { ValidationMessageComponent } from './validation-message/validation-message.component';
import { ModulesComponent } from './modules/modules.component';
import { ModulesCreateComponent } from './modules/modules-create/modules-create.component';
import { ModulesEditComponent } from './modules/modules-edit/modules-edit.component';
import { EntityComponent } from './entity/entity.component';
import { EntityCreateComponent } from './entity/entity-create/entity-create.component';
import { EntityEditComponent } from './entity/entity-edit/entity-edit.component';
import { DependenceComponent } from './dependence/dependence.component';
import { DependenceEditComponent } from './dependence/dependence-edit/dependence-edit.component';
import { DependenceCreateComponent } from './dependence/dependence-create/dependence-create.component';
import { SerieComponent } from './serie/serie.component';
import { SerieCreateComponent } from './serie/serie-create/serie-create.component';
import { SerieEditComponent } from './serie/serie-edit/serie-edit.component';
import { SubserieCreateComponent } from './subserie/subserie-create/subserie-create.component';
import { SubserieEditComponent } from './subserie/subserie-edit/subserie-edit.component';
import { DoctypeComponent } from './doctype/doctype.component';
import { DoctypeCreateComponent } from './doctype-create/doctype-create.component';
import { DoctypeEditComponent } from './doctype-edit/doctype-edit.component';
import { InventoryComponent } from './inventory/inventory.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    ValidationMessageComponent,
    UsersComponent,
    UserCreateComponent,
    RoleComponent,
    RoleCreateComponent,
    RoleEditComponent,
    DeleteConfirmationComponent,
    ModulesComponent,
    ModulesCreateComponent,
    ModulesEditComponent,
    EntityComponent,
    EntityCreateComponent,
    EntityEditComponent,
    DependenceComponent,
    DependenceEditComponent,
    DependenceCreateComponent,
    SerieComponent,
    SerieCreateComponent,
    SerieEditComponent,
    SubserieCreateComponent,
    SubserieEditComponent,
    DoctypeComponent,
    DoctypeCreateComponent,
    DoctypeEditComponent,
    InventoryComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    AppRoutingModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatInputModule,
    MatNativeDateModule,
    MatDividerModule,
    MatSidenavModule,
    MatToolbarModule,
    MatDatepickerModule,
    MatTreeModule,
    MatRadioModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatSortModule,
    MatSnackBarModule,
    MatProgressBarModule,
    MatMenuModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatTooltipModule,
    MatOptionModule,
    MatExpansionModule,
    HttpModule,
    ReactiveFormsModule,
    FormsModule,
    MatSelectModule,
    DynamicFormsCoreModule.forRoot(),
    DynamicFormsMaterialUIModule,
    NgxMaskModule.forRoot(),
    ServiceWorkerModule.register('/ngsw-worker.js', {
      enabled: environment.production
    })
  ],
  providers: [BaseService, MatIconRegistry],
  entryComponents: [
    UserCreateComponent,
    RoleCreateComponent,
    ModulesCreateComponent,
    ModulesEditComponent,
    RoleEditComponent,
    DeleteConfirmationComponent,
    EntityCreateComponent,
    EntityEditComponent,
    DependenceEditComponent,
    DependenceCreateComponent,
    SerieCreateComponent,
    SerieEditComponent,
    SubserieCreateComponent,
    SubserieEditComponent,
    DoctypeCreateComponent,
    DoctypeEditComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(public matIconRegistry: MatIconRegistry) {
    matIconRegistry.registerFontClassAlias('fontawesome', 'fa');
  }
}
